package net.stealthmc.hgcore.commands;

import com.google.common.collect.ImmutableList;
import net.stealthmc.hgcommon.Permission;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcore.HGBukkitMain;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class HHCommand implements TabExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!sender.hasPermission(Permission.HG_HH)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNoPermission());
			return true;
		}
		if (!(sender instanceof Player)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNotAPlayer());
			return true;
		}
		if (sender.hasPermission(Permission.HG_HEALTH)) {
			((Player) sender).performCommand("health");
		}
		if (sender.hasPermission(Permission.HG_HUNGER)) {
			((Player) sender).performCommand("feed");
		}
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		return ImmutableList.of();
	}
}
