package net.stealthmc.hgcore.commands;

import com.google.common.collect.ImmutableList;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcommon.util.CommandUtils;
import net.stealthmc.hgcore.HGBukkitMain;
import org.bukkit.ChatColor;
import org.bukkit.attribute.Attribute;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Objects;

public class HealthCommand implements TabExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
		if (!(sender instanceof Player)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNotAPlayer());
			return true;
		}

		Player player = (Player) sender;

		double health;

		if (args.length == 0) {
			health = Objects.requireNonNull(player.getAttribute(Attribute.GENERIC_MAX_HEALTH)).getDefaultValue();
		} else {
			Double argHealth = CommandUtils.getDouble(args[0]);
			if (argHealth == null) {
				PlayerUtils.sendMessage(player, ChatColor.RED + "Invalid value for " +
						ChatColor.WHITE + "health" + ChatColor.RED + "!");
				return true;
			}
			if (argHealth > player.getHealth()) {
				PlayerUtils.sendMessage(player, ChatColor.RED + "The maximum value for " +
						ChatColor.WHITE + "health" + ChatColor.RED + " is " +
						ChatColor.WHITE + player.getHealth() + ChatColor.RED + "!");
				return true;
			}
			health = argHealth;
		}

		player.setHealth(health);
		PlayerUtils.sendMessage(player, ChatColor.GREEN + String.format("Your health has been set to " +
				ChatColor.WHITE + "%.2f", health));

		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
		return ImmutableList.of();
	}

}
