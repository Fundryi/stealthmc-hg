package net.stealthmc.hgcore.commands;

import com.google.common.collect.ImmutableList;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcommon.util.CommandUtils;
import net.stealthmc.hgcommon.util.StringUtils;
import net.stealthmc.hgcore.HGBukkitMain;
import net.stealthmc.hgcore.listeners.GameModeListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class GameModeCommand implements TabExecutor {

	public static final Map<UUID, GameMode> PREVIOUS_MODES = new HashMap<>();

	private static final String INVALID_INPUT = ChatColor.RED + "Invalid value for " + ChatColor.WHITE +
			"gamemode" + ChatColor.RED + "!";

	private static final List<String> GAME_MODE_COMPLETIONS = ImmutableList.copyOf(Arrays.stream(GameMode.values())
			.map(Enum::name)
			.map(String::toLowerCase)
			.collect(Collectors.toList()));

	private static final Map<Integer, GameMode> INT_GAME_MODES = new HashMap<>();

	static {
		//GameMode.values() == {CREATIVE, SURVIVAL, ...}
		INT_GAME_MODES.put(0, GameMode.SURVIVAL);
		INT_GAME_MODES.put(1, GameMode.CREATIVE);
		INT_GAME_MODES.put(2, GameMode.ADVENTURE);
		INT_GAME_MODES.put(3, GameMode.SPECTATOR);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length == 2) {
			onSwitchOther(sender, args[0], args[1]);
			return true;
		}

		if (!(sender instanceof Player)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNotAPlayer());
			onSyntax(sender, label);
			return true;
		}

		Player player = (Player) sender;

		if (args.length == 0) {
			onToggle((Player) sender);
			return true;
		}

		if (args.length == 1) {
			onSwitch(player, args[0]);
			return true;
		}

		onSyntax(sender, label);
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		String token = args[args.length - 1];

		if (args.length == 1) {
			return CommandUtils.copyPartialMatches(token.toLowerCase(), GAME_MODE_COMPLETIONS);
		}

		if (args.length == 2) {
			return CommandUtils.completeOnlinePlayers(token);
		}

		return ImmutableList.of();
	}

	private void onSyntax(CommandSender sender, String label) {
		PlayerUtils.sendMessage(sender, CommandUtils.getSyntax(label, ImmutableList.of(
				"[<mode>] [<player>]"
		)));
	}

	private void onToggle(Player player) {
		if (player.getGameMode() != GameMode.CREATIVE) {
			setGameMode(player, GameMode.CREATIVE);
		} else {
			GameMode mode = PREVIOUS_MODES.getOrDefault(player.getUniqueId(), GameMode.SURVIVAL);
			setGameMode(player, mode);
		}
		sendGameModeChangedMessage(player);
	}

	private void onSwitch(Player player, String arg) {
		GameMode mode = getGameModeFromArg(arg);

		if (mode == null) {
			PlayerUtils.sendMessage(player, INVALID_INPUT);
			return;
		}

		setGameMode(player, mode);

		sendGameModeChangedMessage(player);
	}

	private void onSwitchOther(CommandSender sender, String arg, String targetName) {
		Player target = Bukkit.getPlayerExact(targetName);
		if (target == null) {
			PlayerUtils.sendMessage(sender, ChatColor.RED + "Player " + ChatColor.WHITE +
					targetName + ChatColor.RED + " does not exist.");
			return;
		}

		GameMode mode = getGameModeFromArg(arg);

		if (mode == null) {
			PlayerUtils.sendMessage(sender, INVALID_INPUT);
			return;
		}

		setGameMode(target, mode);
		sendGameModeChangedMessage(target);

		String name = StringUtils.normalizeEnumName(mode);

		PlayerUtils.sendMessage(sender, ChatColor.GOLD + target.getName() + ChatColor.GREEN +
				" is now in " + ChatColor.GOLD + name + ChatColor.GREEN + " mode.");
	}

	private GameMode getGameModeFromArg(String arg) {
		GameMode gameMode;
		Integer integer = CommandUtils.getInteger(arg);

		if (integer == null) {
			try {
				gameMode = GameMode.valueOf(arg.toUpperCase());
			} catch (Exception ignored) {
				return null;
			}
		} else {
			gameMode = INT_GAME_MODES.get(integer);
		}

		return gameMode;
	}

	private void setGameMode(Player player, GameMode mode) {
		GameMode oldMode = player.getGameMode();

		if (oldMode == mode) {
			return;
		}

		if (oldMode != GameMode.CREATIVE) {
			PREVIOUS_MODES.put(player.getUniqueId(), oldMode);
		}

		GameModeListener.EVENT_IGNORED_UUIDS.add(player.getUniqueId());
		player.setGameMode(mode);
	}

	private void sendGameModeChangedMessage(Player player) {
		GameMode mode = player.getGameMode();
		String name = StringUtils.normalizeEnumName(mode);
		PlayerUtils.sendMessage(player, ChatColor.GREEN + "You are now playing in " +
				ChatColor.GOLD + name + ChatColor.GREEN + " mode.");
	}

}
