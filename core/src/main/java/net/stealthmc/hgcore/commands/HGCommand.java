package net.stealthmc.hgcore.commands;

import com.google.common.collect.ImmutableList;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.Permission;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcommon.util.CommandUtils;
import net.stealthmc.hgcore.HGBukkitMain;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgcore.game.gamephase.GamePhase;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class HGCommand implements TabExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length == 0) {
			onSyntax(sender, label);
			return true;
		}

		if (args[0].equalsIgnoreCase("keepworld")) {
			onKeepworld(sender, CommandUtils.remainingArgs(args, 1));
			return true;
		}

		if (args[0].equalsIgnoreCase("timer")) {
			onTimer(sender, CommandUtils.remainingArgs(args, 1));
			return true;
		}

		if (args[0].equalsIgnoreCase("build")) {
			onBuild(sender);
			return true;
		}

		if (args[0].equalsIgnoreCase("forcefinal")) {
			onForcefinal(sender);
			return true;
		}

		if (args[0].equalsIgnoreCase("pvp")) {
			onPvPToggle(sender);
			return true;
		}

		onSyntax(sender, label);
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args) {
		String token = args[args.length - 1];

		if (args.length == 1) {
			return CommandUtils.copyPartialMatches(token, ImmutableList.of("keepworld", "timer", "build", "forcefinal", "pvp"));
		}

		if (args.length == 2 && args[0].equalsIgnoreCase("keepworld")) {
			return CommandUtils.completeBooleanValues(token);
		}

		return ImmutableList.of();
	}

	private void onSyntax(CommandSender sender, String alias) {
		PlayerUtils.sendMessage(sender, CommandUtils.getSyntax(alias, ImmutableList.of(
				"keepworld <Yes|No>",
				"timer <seconds>",
				"build"
		)));
	}

	private void onKeepworld(CommandSender sender, String[] args) {
		if (!sender.hasPermission(Permission.HG_KEEPWORLD)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNoPermission());
			return;
		}
		boolean keep = true;
		if (args.length == 1) {
			Boolean argBool = CommandUtils.getBoolean(args[0]);
			if (argBool == null) {
				PlayerUtils.sendMessage(sender, CC.red + "Invalid value for " +
						CC.white + "<Yes|No>" + CC.red + ".");
				return;
			}
			keep = argBool;
		}
		if (keep) {
			PlayerUtils.sendMessage(sender, CC.green + "The world will be " +
					CC.dGreen + "kept" + CC.green + " on shutdown.");
		} else {
			PlayerUtils.sendMessage(sender, CC.green + "The world will be " +
					CC.dRed + "deleted" + CC.green + " on shutdown.");
		}
		HGBukkitMain.getInstance().setKeepWorld(keep);
	}

	private void onTimer(CommandSender sender, String[] args) {
		if (!sender.hasPermission(Permission.HG_TIMER)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNoPermission());
			return;
		}
		if (args.length == 0) {
			args = new String[]{"0"};
		}
		Integer time = CommandUtils.getInteger(args[0]);
		if (time == null) {
			PlayerUtils.sendMessage(sender, CC.red + "Invalid value for " + CC.white + "seconds" + CC.red + "!");
			return;
		}
		if(!(HGBukkitMain.getInstance().getOnlinePlayers().size() > 1)){
			return;
		}
		GameHandler handler = GameHandler.getInstance();
		handler.getGameTimer().setTime(time);
	}

	private void onBuild(CommandSender sender) {
		if (!sender.hasPermission(Permission.HG_BUILD)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNoPermission());
			return;
		}
		if (!(sender instanceof Player)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNotAPlayer());
			return;
		}
		Player player = (Player) sender;
		if (GameHandler.getInstance().getPermittedBuilders().add(player.getUniqueId())) {
			PlayerUtils.sendMessage(player, CC.green + "You are now able to build.");
		} else {
			GameHandler.getInstance().getPermittedBuilders().remove(player.getUniqueId());
			PlayerUtils.sendMessage(player, CC.red + "You are no longer able to build.");
		}
	}

	private void onForcefinal(CommandSender sender) {
		if (!sender.hasPermission(Permission.HG_TIMER)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNoPermission());
			return;
		}
		GameHandler handler = GameHandler.getInstance();
		while (handler.getCurrentPhase().ordinal() < GamePhase.SHOWDOWN.ordinal()) {
			handler.nextPhase();
		}
	}

	private void onPvPToggle(CommandSender sender) {
		if (!sender.hasPermission(Permission.HG_PVPTOGGLE)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNoPermission());
			return;
		}
		if (!(sender instanceof Player)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNotAPlayer());
			return;
		}
		Player player = (Player) sender;
		if (GameHandler.getInstance().getGameWorld().getPVP()) {
			GameHandler.getInstance().getGameWorld().setPVP(false);
			PlayerUtils.sendMessage(player, CC.red + "Global PvP is now disabled.");
		} else {
			GameHandler.getInstance().getGameWorld().setPVP(true);
			PlayerUtils.sendMessage(player, CC.green + "Global PvP is now enabled.");
		}
	}

}
