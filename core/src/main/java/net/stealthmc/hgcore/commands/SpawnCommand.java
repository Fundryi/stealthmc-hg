package net.stealthmc.hgcore.commands;

import com.google.common.collect.ImmutableList;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.Permission;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcore.HGBukkitMain;
import net.stealthmc.hgcore.game.GameHandler;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class SpawnCommand implements TabExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		GameHandler handler = GameHandler.getInstance();

		if (!sender.hasPermission(Permission.HG_SPAWN)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNoPermission());
			return true;
		}
		if (!(sender instanceof Player)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNotAPlayer());
			return true;
		}

		if (!handler.getCurrentPhase().isJoiningEnabled()) {
			PlayerUtils.sendMessage(sender, ChatColor.RED + "Can only be used before the game starts!");
			return true;
		}

		//Location randomLocation = handler.getRandomLocation();
		//((Player) sender).teleport(randomLocation);

		//PlayerUtils.sendMessage(sender, ChatColor.GREEN + "You have been randomly teleported to a new place within the border.");
		PlayerUtils.sendMessage(sender, CC.red + "This command is currently disabled because of a bug! Sorry.");
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		return ImmutableList.of();
	}
}
