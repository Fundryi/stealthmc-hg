package net.stealthmc.hgcore.commands;

import com.google.common.collect.ImmutableList;
import net.stealthmc.hgcommon.Permission;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcommon.util.CommandUtils;
import net.stealthmc.hgcommon.util.Menu;
import net.stealthmc.hgcore.HGBukkitMain;
import net.stealthmc.hgcore.service.HGMutableService;
import net.stealthmc.hgcore.service.HGService;
import net.stealthmc.hgcore.service.ServiceHandler;
import net.stealthmc.hgcore.service.helper.MutableServiceInputValidator;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.List;

public class HGSettingsCommand implements TabExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length == 0) {
			onOverview(sender);
			return true;
		}

		if (!sender.hasPermission(Permission.HGSETTINGS_ADMIN)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNoPermission());
			return true;
		}

		if (args.length != 2) {
			onSyntax(sender, label);
			return true;
		}

		String key = args[0];
		String value = args[1];

		HGService hgService = ServiceHandler.getSubService(key);

		if (hgService == null) {
			PlayerUtils.sendMessage(sender, ChatColor.RED + "This service does not exist.");
			return true;
		}

		if (!(hgService instanceof HGMutableService)) {
			PlayerUtils.sendMessage(sender, ChatColor.RED + "This service cannot be changed.");
			return true;
		}

		MutableServiceInputValidator validator = ((HGMutableService) hgService).getInputValidator();
		if (!validator.acceptInput(value)) {
			PlayerUtils.sendMessage(sender, ChatColor.RED + validator.getErrorMessage());
			return true;
		}

		PlayerUtils.sendMessage(sender, ChatColor.GREEN + "Value was set successfully.");
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		return ImmutableList.of();
	}

	private void onSyntax(CommandSender sender, String label) {
		PlayerUtils.sendMessage(sender, CommandUtils.getSyntax(label, ImmutableList.of("<key> <value>")));
	}

	private void onOverview(CommandSender sender) {
		Menu menu = new Menu(ChatColor.GREEN + "Game settings:");
		ServiceHandler.getRegisteredServicesPrintable().forEach(menu::addSub);
		PlayerUtils.sendMessage(sender, menu.toComponents());
	}

}
