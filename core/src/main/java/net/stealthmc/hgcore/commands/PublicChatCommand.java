package net.stealthmc.hgcore.commands;

import com.google.common.collect.ImmutableList;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcommon.util.CommandUtils;
import net.stealthmc.hgcore.HGBukkitMain;
import net.stealthmc.hgcore.listeners.ChatListener;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.HashSet;

public class PublicChatCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!(sender instanceof Player)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNotAPlayer());
			return true;
		}

		if (args.length == 0) {
			onSyntax(sender, label);
			return true;
		}

		String msg = CommandUtils.joinArgs(args, 0, args.length, " ");

		Player player = (Player) sender;

		ChatListener.setStaffChattingOnce(player.getUniqueId());

		AsyncPlayerChatEvent event = new AsyncPlayerChatEvent(false, player, msg,
				new HashSet<>(Bukkit.getOnlinePlayers()));
		Bukkit.getPluginManager().callEvent(event);

		if (event.isCancelled()) {
			return true;
		}

		String format = String.format(event.getFormat(), player.getDisplayName(), event.getMessage());

		Bukkit.broadcastMessage(format);
		return true;
	}

	private void onSyntax(CommandSender sender, String label) {
		PlayerUtils.sendMessage(sender, CommandUtils.getSyntax(label, ImmutableList.of("<message>")));
	}
}
