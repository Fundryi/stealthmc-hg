package net.stealthmc.hgcore.listeners;

import com.destroystokyo.paper.event.player.PlayerInitialSpawnEvent;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.ExpBottleEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;

public class PlayerListener implements Listener {

	@EventHandler(priority = EventPriority.HIGH)
	public void onLogin(PlayerLoginEvent event) {
		if (event.getResult() != PlayerLoginEvent.Result.ALLOWED) {
			return;
		}
		GameHandler.getInstance().handleLogin(event);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onJoin(PlayerJoinEvent event) {
		event.setJoinMessage(null);
		GameHandler.getInstance().handleJoin(event);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onInitialSpawn(PlayerInitialSpawnEvent event) {
		GameHandler.getInstance().handleInitialSpawn(event);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onDisconnect(PlayerQuitEvent event) {
		event.setQuitMessage(null);
		GameHandler.getInstance().handleQuit(event);
		HGPlayer hgPlayer = HGPlayer.from(event.getPlayer());
		hgPlayer.onQuit();
	}

	@EventHandler
	public void onEntityDamageByBlock(EntityDamageByBlockEvent event) {
		if (event.getEntity() instanceof Player) {
			GameHandler.getInstance().handlePlayerDamageByBlock(event);
		}
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		GameHandler.getInstance().handleDeath(event);
	}

	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		GameHandler.getInstance().handleHunger(event);
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		GameHandler.getInstance().handleBlockBreak(event);
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		GameHandler.getInstance().handleBlockPlace(event);
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		GameHandler.getInstance().handleInteract(event);
	}

	@EventHandler
	public void onInteractEntity(PlayerInteractEntityEvent event) {
		GameHandler.getInstance().handleInteractEntity(event);
	}

	@EventHandler
	public void onTeleport(PlayerTeleportEvent event) {
		GameHandler.getInstance().handleTeleport(event);
	}

	@EventHandler
	public void onCraftItem(CraftItemEvent event) {
		GameHandler.getInstance().handleCraftItem(event);
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent event) {
		GameHandler.getInstance().handleDropItem(event);
	}

	@EventHandler
	public void onFish(PlayerFishEvent event) {
		GameHandler.getInstance().handleFish(event);
	}

	@EventHandler
	public void onItemDamage(PlayerItemDamageEvent event) {
		GameHandler.getInstance().handleItemDamage(event);
	}

	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		GameHandler.getInstance().handleMove(event);
	}

	@EventHandler
	public void onSneak(PlayerToggleSneakEvent event) {
		GameHandler.getInstance().handleSneak(event);
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		GameHandler.getInstance().handleInventoryClick(event);
	}

	@EventHandler
	public void onPlayerExpChange(PlayerExpChangeEvent event) {
		GameHandler.getInstance().handlePlayerExpChange(event);
	}

	@EventHandler
	public void onExpBottle(ExpBottleEvent event) {
		GameHandler.getInstance().handleExpBottle(event);
	}

}
