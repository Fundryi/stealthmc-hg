package net.stealthmc.hgcore.listeners;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import net.stealthmc.hgcore.service.ServiceHandler;
import net.stealthmc.hgcore.service.predefined.IntegerService;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class SoupLimitListener implements Listener, IntegerService {

	public static final String SERVICE_KEY = "craft-carry-limit";
	private static DatedMaterial MUSHROOM_SOUP_DATED = new DatedMaterial(Material.MUSHROOM_STEW, 0);
	private int craftingLimit = -1;  //DEFAULT OFF
	private Set<Map<DatedMaterial, Integer>> limitedMaterials;

	public SoupLimitListener() {
		limitedMaterials = new HashSet<>();
		for (Recipe recipe : Bukkit.getRecipesFor(new ItemStack(Material.MUSHROOM_STEW))) {
			Map<DatedMaterial, Integer> ingredients = new HashMap<>();
			List<DatedMaterial> mats;
			if (recipe instanceof ShapelessRecipe) {
				mats = ((ShapelessRecipe) recipe).getIngredientList()
						.stream()
						.filter(itemStack -> itemStack.getType() != Material.BOWL)
						.map(SoupLimitListener::wrapItemStack)
						.collect(Collectors.toList());
			} else if (recipe instanceof ShapedRecipe) {
				mats = ((ShapedRecipe) recipe).getIngredientMap()
						.values()
						.stream()
						.filter(itemStack -> itemStack.getType() != Material.BOWL)
						.map(SoupLimitListener::wrapItemStack)
						.collect(Collectors.toList());
			} else {
				continue;
			}
			mats.forEach(material -> {
				ingredients.compute(material, (_m, count) -> {
					if (count == null) {
						return 1;
					}
					return count + 1;
				});
			});
			limitedMaterials.add(ingredients);
		}

		ServiceHandler.registerService(SERVICE_KEY, this);
	}

	private static DatedMaterial wrapItemStack(ItemStack in) {
		return new DatedMaterial(in.getType(), in.getData().getData()); //DEPRECATED NEED UPDATE
	}

	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onItemPickupTry(EntityPickupItemEvent event) {
		if (craftingLimit < 0) {
			return;
		}

		Item item = event.getItem();

		if (item == null) {
			return;
		}

		ItemStack itemStack = item.getItemStack();

		if (itemStack == null) {
			return;
		}

		DatedMaterial datedMaterial = wrapItemStack(itemStack);

		if (!isRelevantMaterial(datedMaterial)) {
			return;
		}

		if (!(event.getEntity() instanceof Player)) {
			return;
		}

		Player player = (Player) event.getEntity();

		Map<DatedMaterial, Integer> relevantContents = getRelevantContents(player.getInventory());
		int soupIndex = calculateSoupIndex(relevantContents);

		relevantContents.compute(datedMaterial, (material, integer) -> {
			if (integer == null) {
				return itemStack.getAmount();
			}
			return integer + itemStack.getAmount();
		});

		int afterPickupIndex = calculateSoupIndex(relevantContents);

		if (afterPickupIndex > soupIndex && afterPickupIndex > craftingLimit) {
			event.setCancelled(true);

			int delta = craftingLimit - soupIndex;
			itemStack.setAmount(itemStack.getAmount() - delta);
			item.setItemStack(itemStack);

			ItemStack clone = itemStack.clone();
			clone.setAmount(delta);

			player.getInventory().addItem(clone);
		}
	}

	private Map<DatedMaterial, Integer> getRelevantContents(Inventory inventory) {
		Map<DatedMaterial, Integer> contents = new HashMap<>();

		for (ItemStack itemStack : inventory.getContents()) {
			if (itemStack == null) {
				continue;
			}

			DatedMaterial datedMaterial = wrapItemStack(itemStack);

			if (isRelevantMaterial(datedMaterial)) {
				contents.compute(datedMaterial, (material, integer) -> {
					if (integer == null) {
						return itemStack.getAmount();
					}
					return integer + itemStack.getAmount();
				});
			}
		}

		return contents;
	}

	private int calculateSoupIndex(Map<DatedMaterial, Integer> contents) {
		int soupCount = contents.getOrDefault(MUSHROOM_SOUP_DATED, 0);

		for (val limit : limitedMaterials) {
			int localLow = Integer.MAX_VALUE;
			for (val in : limit.entrySet()) {
				int count = contents.getOrDefault(in.getKey(), 0);
				int neededPer = in.getValue();
				localLow = Math.min(localLow, count / neededPer);
			}
			soupCount += localLow;
		}

		return soupCount;
	}

	private boolean isRelevantMaterial(DatedMaterial in) {
		return MUSHROOM_SOUP_DATED.equals(in) ||
				limitedMaterials.stream().anyMatch(materialIntegerMap ->
						materialIntegerMap.containsKey(in));
	}

	@Override
	public Integer get() {
		return craftingLimit;
	}

	@Override
	public void set(Integer value) {
		craftingLimit = value;
	}

	@AllArgsConstructor
	@Data
	@EqualsAndHashCode(of = {"material", "byteData"})
	private static class DatedMaterial {
		Material material;
		int byteData;
	}

}
