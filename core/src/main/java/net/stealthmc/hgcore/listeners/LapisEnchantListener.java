package net.stealthmc.hgcore.listeners;

import net.stealthmc.hgcommon.bukkit.inventory.ItemStackUtils;
import net.stealthmc.hgcore.HGBukkitMain;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.EnchantingInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class LapisEnchantListener implements Listener {

	private static final ItemStack LAPIS = ItemStackUtils.createItemStack(Material.INK_SAC, (byte) 4,
			ChatColor.BLUE + "Untouchable Lapis Lazuli");

	static {
		LAPIS.setAmount(3);
	}

	@EventHandler
	public void onInventoryOpen(InventoryOpenEvent event) {
		Inventory inventory = event.getInventory();
		if (inventory instanceof EnchantingInventory) {
			((EnchantingInventory) inventory).setSecondary(LAPIS);
		}
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		Inventory inventory = event.getInventory();
		if (inventory instanceof EnchantingInventory) {
			((EnchantingInventory) inventory).setSecondary(null);
		}
	}

	@EventHandler
	public void onClick(InventoryClickEvent event) {
		Inventory inventory = event.getClickedInventory();
		if (!(inventory instanceof EnchantingInventory)) {
			return;
		}
		if (!LAPIS.isSimilar(event.getCurrentItem())) {
			return;
		}
		event.setCancelled(true);
	}

	@EventHandler
	public void onEnchant(EnchantItemEvent event) {
		Inventory inventory = event.getInventory();
		if (inventory instanceof EnchantingInventory) {
			Bukkit.getScheduler().runTaskLater(HGBukkitMain.getInstance(), () -> {
				((EnchantingInventory) inventory).setSecondary(LAPIS);
			}, 1L);
		}
	}

}
