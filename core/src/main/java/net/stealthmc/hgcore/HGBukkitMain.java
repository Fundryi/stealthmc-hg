package net.stealthmc.hgcore;

import lombok.Getter;
import lombok.Setter;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.stealthmc.hgcommon.Permission;
import net.stealthmc.hgcommon.bukkit.BukkitGlobalPlayer;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcommon.bukkit.inventory.InventoryMenu;
import net.stealthmc.hgcommon.handler.GlobalPlayer;
import net.stealthmc.hgcommon.handler.IHGMain;
import net.stealthmc.hgcommon.handler.PluginHandler;
import net.stealthmc.hgcommon.util.PhrasePair;
import net.stealthmc.hgcore.commands.*;
import net.stealthmc.hgcore.config.Messages;
import net.stealthmc.hgcore.game.CraftingRecipeHandler;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgcore.game.WorldHandler;
import net.stealthmc.hgcore.game.gamephase.GamePhase;
import net.stealthmc.hgcore.game.gamephase.GameRuleGamePhaseUpdateHandler;
import net.stealthmc.hgcore.game.gamephase.MotdGamePhaseUpdateHandler;
import net.stealthmc.hgcore.listeners.*;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.Collection;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class HGBukkitMain extends JavaPlugin implements IHGMain {

	public static final String HG_WORLD_NAME = "stealthmc-HG";
	public static final PhrasePair SECONDS_PHRASES = new PhrasePair("second", "seconds");
	@Getter
	private static HGBukkitMain instance;
	@Getter
	private Messages messages;
	@Getter
	@Setter
	private boolean keepWorld = false;

	public static String formatSeconds(int seconds) {
		return String.format("%02d:%02d", seconds / 60, seconds % 60);
	}

	@Override
	public void onEnable() {
		instance = this;
		PluginHandler.setHgMain(this);
		InventoryMenu.init(this);

		File keepWorldFlagFile = new File(getDataFolder(), "keepworld");
		if (keepWorldFlagFile.exists()) {
			keepWorld = true;
		}

		try {
			messages = new Messages(this);
		} catch (InvalidConfigurationException e) {
			getLogger().log(Level.SEVERE, "Error starting stealthmc-hg-core!", e);
			return;
		}

		//Recipes have to be registered before Listeners are initialized.
		CraftingRecipeHandler.registerSoupRecipes();

		registerCommands();
		registerListeners();

		new MotdGamePhaseUpdateHandler().attachTo(GameHandler.getInstance());

		Bukkit.getScheduler().runTaskLater(this, this::readyWorld, 20L);
	}

	private void readyWorld() {
		World world = Bukkit.getWorld(HG_WORLD_NAME);
		if (world == null) {
			getLogger().log(Level.INFO, "Creating a new HG-World named \"" + HG_WORLD_NAME + "\"");
			world = WorldHandler.createGameWorld();
			if (world == null) {
				getLogger().log(Level.SEVERE, "World" + HG_WORLD_NAME + " could not be created!");
				return;
			}
		} else {
			//getLogger().log(Level.INFO, "An HG-World already existed. It will be deleted unless saved via command.");
			getLogger().log(Level.INFO, "An HG-World already existed. It will not be deleted unless forced via command.");
			//keepWorld = true;
		}
		GameHandler gameHandler = GameHandler.getInstance();

		//Phase -> Lobby
		gameHandler.setGameWorld(world);

		new GameRuleGamePhaseUpdateHandler().attachTo(gameHandler);
	}

	private void registerCommands() {
		registerCommand("hg", new HGCommand(), Permission.HG_COMMAND);
		registerCommand("respawn", new RespawnCommand(), Permission.HG_RESPAWN);
		registerCommand("hunger", new HungerCommand(), Permission.HG_HUNGER);
		registerCommand("health", new HealthCommand(), Permission.HG_HEALTH);
		registerCommand("publicchat", new PublicChatCommand(), Permission.PUBLICCHAT);
		registerCommand("hgsettings", new HGSettingsCommand(), Permission.HGSETTINGS_OVERVIEW);
		registerCommand("gamemode", new GameModeCommand(), Permission.HG_GAMEMODE);
		registerCommand("spawn", new SpawnCommand(), Permission.HG_SPAWN);
		registerCommand("sspawn", new SSpawnCommand(), Permission.HG_SSPAWN);
		registerCommand("hh", new HHCommand(), Permission.HG_HH);
	}

	private void registerCommand(String name, CommandExecutor executor, String permission) {
		PluginCommand command = getCommand(name);
		if (command == null) {
			getLogger().log(Level.WARNING, "Tried to register unknown command: " + name);
			return;
		}
		command.setExecutor(executor);
		command.setPermission(permission);
		command.setPermissionMessage(PlayerUtils.colorize(messages.getNoPermission()));
	}

	private void registerListeners() {
		PluginManager manager = Bukkit.getPluginManager();
		manager.registerEvents(new PlayerListener(), this);
		manager.registerEvents(new EntityListener(), this);
		manager.registerEvents(new ChatListener(), this);
		manager.registerEvents(new FurnaceSmeltListener(), this);
		manager.registerEvents(new ItemCreateListener(), this);
		manager.registerEvents(new GameModeListener(), this);
		manager.registerEvents(new LapisEnchantListener(), this);
		manager.registerEvents(new SoupCraftListener(), this);
		manager.registerEvents(new SoupLimitListener(), this);
	}

	public void onDisable() {
		GameHandler.getInstance().kickAllPlayers();
		GameHandler.getInstance().setCurrentPhase(GamePhase.END);

		CraftingRecipeHandler.removeAddedRecipes();
		if (keepWorld) {
			getLogger().log(Level.INFO, "World is not going to be deleted!");
			return;
		}
		if (!WorldHandler.unloadAndDelete(GameHandler.getInstance().getGameWorld())) {
			getLogger().log(Level.SEVERE, "World \"" + HG_WORLD_NAME + "\" could not be deleted!");
			return;
		}
		getLogger().log(Level.INFO, "World \"" + HG_WORLD_NAME + "\" has been deleted.");
	}

	@Override
	public Collection<? extends GlobalPlayer> getOnlinePlayers() {
		return Bukkit.getOnlinePlayers().stream()
				.map(BukkitGlobalPlayer::new)
				.collect(Collectors.toList());
	}
}
