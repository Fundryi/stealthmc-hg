package net.stealthmc.hgcore.api;

import com.google.common.collect.Maps;
import lombok.AllArgsConstructor;
import lombok.Data;
import net.stealthmc.hgcommon.util.Pair;
import net.stealthmc.hgcommon.util.StreamUtils;
import net.stealthmc.hgcore.HGBukkitMain;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.spigotmc.event.entity.EntityDismountEvent;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

@Data
@AllArgsConstructor
public abstract class AttachedListener {

	//Class -> MethodNames; MethodName -> {Method, Priority, ForeignHandler}
	private static Map<Class, Map<String, Pair<Method, Pair<Integer, Boolean>>>> methodCache = Maps.newHashMap();
	private UUID entityId;

	public static void invokeInPriorityOrder(List<AttachedListener> adapters,
											 String methodName,
											 UUID[] participatingUUIDs,
											 Class<?>[] parameterTypes,
											 Object... args) {
		Map<Method, Pair<AttachedListener, Pair<Integer, Boolean>>> priorityMap = Maps.newHashMap();

		for (AttachedListener adapter : adapters) {
			Class<? extends AttachedListener> adapterClass = adapter.getClass();

			Pair<Method, Pair<Integer, Boolean>> cachedPair = methodCache.compute(adapterClass, (_c, methodMap) -> {
				if (methodMap == null) {
					methodMap = Maps.newHashMap();
				}
				methodMap.compute(methodName, (_m, pair) -> {
					if (pair == null) {
						try {
							Method method = adapterClass.getMethod(methodName, parameterTypes);
							method.setAccessible(true);

							int priority = AdapterPriority.DEFAULT_PRIORITY;
							AdapterPriority adapterPriorityAnnotation = method.getAnnotation(AdapterPriority.class);
							if (adapterPriorityAnnotation != null) {
								priority = adapterPriorityAnnotation.priority();
							}

							boolean foreignAdapter = method.getAnnotation(ForeignAdapter.class) != null;

							pair = Pair.of(method, Pair.of(priority, foreignAdapter));
						} catch (NoSuchMethodException e) {
							throw new IllegalArgumentException(e);
						}
					}
					return pair;
				});
				return methodMap;
			}).get(methodName);

			priorityMap.put(cachedPair.getKey(), Pair.of(adapter, cachedPair.getValue()));
		}

		priorityMap.entrySet().stream()
				.sorted(StreamUtils.comparingIntDescending(value -> value.getValue().getValue().getKey()))
				.forEach(entry -> {
					boolean foreignAdapter = entry.getValue().getValue().getValue();
					AttachedListener listener = entry.getValue().getKey();
					if (!foreignAdapter && !ArrayUtils.contains(participatingUUIDs, listener.getEntityId())) {
						return;
					}
					try {
						entry.getKey().invoke(entry.getValue().getKey(), args);
					} catch (IllegalAccessException | InvocationTargetException e) {
						HGBukkitMain.getInstance().getLogger().log(Level.SEVERE, "An error occurred while handling the event for listener: " + listener.getClass().getName(), e);
					}
				});
	}

	public void onBattleReady() {

	}

	public void onInteract(PlayerInteractEvent event) {

	}

	public void onInteractEntity(PlayerInteractEntityEvent event) {

	}

	public void onBlockBreak(BlockBreakEvent event) {

	}

	public void onBlockPlace(BlockPlaceEvent event) {

	}

	public void onDeath(PlayerDeathEvent event) {

	}

	public void onItemDrop(PlayerDropItemEvent event) {

	}

	/**
	 * Handle incoming damage
	 *
	 * @param event Event
	 */
	public void onDamage(EntityDamageEvent event) {

	}

	/**
	 * Handle incoming <b><u>and</u></b> outgoing damage to/from other entities
	 *
	 * @param event Event
	 */
	public void onEntityDamage(EntityDamageByEntityEvent event) {

	}

	public void onItemCraft(CraftItemEvent event) {

	}

	public void onFish(PlayerFishEvent event) {

	}

	public void onItemDamage(PlayerItemDamageEvent event) {

	}

	public void onMove(PlayerMoveEvent event) {

	}

	public void onTeleport(PlayerTeleportEvent event) {

	}

	public void onSneak(PlayerToggleSneakEvent event) {

	}

	public void onProjectileLaunch(ProjectileLaunchEvent event) {

	}

	public void onProjectileHit(ProjectileHitEvent event) {

	}

	public void onEntityDeath(EntityDeathEvent event) {

	}

	public void onInventoryClick(InventoryClickEvent event) {

	}

	public void onDismount(EntityDismountEvent event) {

	}

	public void onExpBottle(ExpBottleEvent event) {

	}

	public void onPlayerExpChange(PlayerExpChangeEvent event) {

	}

}
