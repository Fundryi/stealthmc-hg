package net.stealthmc.hgcore.game.util;

import lombok.Getter;
import lombok.Setter;
import net.stealthmc.hgcore.game.GameHandler;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.entity.Player;

import java.util.function.Consumer;

public class BorderManager {

	private static final double MAX_Y = 132.0;

	private final World world;
	@Getter
	@Setter
	public Consumer<Double> borderDiameterChangedConsumer;
	private double borderDamageDistance = GameHandler.WORLDBORDER_DAMAGE_RADIUS;
	@Getter
	private double diameter = GameHandler.WORLDBORDER_INITIAL_SIZE;

	public BorderManager(World world) {
		this.world = world;

		WorldBorder border = world.getWorldBorder();
		border.setCenter(0, 0);
		border.setSize(diameter);
		border.setWarningDistance(14);
	}

	public void setDiameter(double diameter) {
		this.diameter = diameter;
		WorldBorder worldBorder = world.getWorldBorder();
		worldBorder.setSize(diameter);
		if (borderDiameterChangedConsumer != null) {
			borderDiameterChangedConsumer.accept(diameter);
		}
	}

	public void setBorderDamageDistance(double distance) {
		this.borderDamageDistance = distance;
	}

	public void setCenter(double x, double y) {
		WorldBorder worldBorder = world.getWorldBorder();
		worldBorder.setCenter(x, y);
	}

	/**
	 * Used to calculate the should be value of the diameter according to
	 * playerCount
	 * <p>
	 * This method does not change anything
	 *
	 * @param playerCount How many players are in the game
	 * @return the should be value of the diameter
	 */
	public int calculateDiameter(int playerCount) {
		int result = 150;
		if (playerCount >= 200) {
			result = 750;
		} else if (playerCount >= 150) {
			result = 600;
		} else if (playerCount >= 90) {
			result = 500;
		} else if (playerCount >= 80) {
			result = 450;
		} else if (playerCount >= 70) {
			result = 400;
		} else if (playerCount >= 60) {
			result = 350;
		} else if (playerCount >= 50) {
			result = 300;
		} else if (playerCount >= 40) {
			result = 250;
		} else if (playerCount >= 30) {
			result = 200;
		}
		return result * 2; //Return diameter
	}

	/**
	 * Uses the border damage distance to check.
	 *
	 * @param player The player
	 * @return true if the player is near the border
	 */
	public boolean isNearBorder(Player player) {
		WorldBorder border = world.getWorldBorder();

		Location playerLocation = player.getLocation();
		if (!playerLocation.getWorld().equals(world)) {
			return false;
		}

		if (playerLocation.getY() >= MAX_Y) {
			return true;
		}

		double radius = border.getSize() / 2;
		double distX = playerLocation.getX() - border.getCenter().getX();
		double distZ = playerLocation.getZ() - border.getCenter().getZ();

		return !(Math.abs(distX) < radius - borderDamageDistance)
				|| !(Math.abs(distZ) < radius - borderDamageDistance);
	}

	public int getCenterX() {
		return world.getWorldBorder().getCenter().getBlockX();
	}

	public int getCenterZ() {
		return world.getWorldBorder().getCenter().getBlockZ();
	}

}
