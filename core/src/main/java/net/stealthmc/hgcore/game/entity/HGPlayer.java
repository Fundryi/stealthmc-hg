package net.stealthmc.hgcore.game.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import net.stealthmc.hgcore.api.AttachedListener;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.entity.ExpBottleEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@Getter
@Setter
public class HGPlayer extends HGEntity {

	public static final Object SPECTATE_LOCK = new Object();
	@Getter(value = AccessLevel.PRIVATE)
	@Setter(value = AccessLevel.PRIVATE)
	private Player playerRef = null;
	@Setter
	private int kills;
	@Setter
	private Location logoutLocation = null;
	@Setter
	private UUID playerCompassTarget = null;
	@Setter
	private UUID lastDamager = null;
	private @Nullable

	Entity playerHalter = null;
	/*DO NOT MODIFY THESE!! HANDLED BY GAMEHANDLER*/
	private boolean initialPlayer = false;
	private boolean spectating = false;
	private boolean hidden = false;
	private boolean halted = false;

	protected HGPlayer(UUID entityId) {
		super(entityId);
	}

	public static HGPlayer from(UUID uuid) {
		return (HGPlayer) cache.compute(uuid, (uuid1, hgPlayer) -> {
			if (hgPlayer == null) {
				hgPlayer = new HGPlayer(uuid);
			}
			return hgPlayer;
		});
	}

	public static HGPlayer from(Player player) {
		HGPlayer hgPlayer = from(player.getUniqueId());
		if (hgPlayer.getPlayerRef() == null) {
			hgPlayer.setPlayerRef(player);
		}
		return hgPlayer;
	}

	public static Collection<HGPlayer> allKnownPlayers() {
		return cache.values().stream()
				.filter(e -> e instanceof HGPlayer)
				.map(e -> (HGPlayer) e)
				.collect(Collectors.toList());
	}

	public void setSpectating(boolean spectating) {
		synchronized (SPECTATE_LOCK) {
			this.spectating = spectating;
		}
	}

	public @Nullable
	Player getPlayer() {
		if (playerRef != null) {
			return playerRef;
		}
		playerRef = Bukkit.getPlayer(entityId);
		return playerRef;
	}

	public void onQuit() {
		playerRef = null;
	}

	public void onBattleReady() {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onBattleReady",
				new UUID[]{getEntityId()}, new Class[0]);
		//this.getAttachedListeners().forEach(AttachedListener::onBattleReady);
	}

	public void onInteract(PlayerInteractEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onInteract", participants,
				new Class[]{PlayerInteractEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onInteract(event));
	}

	public void onInteractEntity(PlayerInteractEntityEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onInteractEntity", participants,
				new Class[]{PlayerInteractEntityEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onInteractEntity(event));
	}

	public void onBlockBreak(BlockBreakEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onBlockBreak", participants,
				new Class[]{BlockBreakEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onBlockBreak(event));
	}

	public void onBlockPlace(BlockPlaceEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onBlockPlace", participants,
				new Class[]{BlockPlaceEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onBlockPlace(event));
	}

	public void onDeath(PlayerDeathEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onDeath", participants,
				new Class[]{PlayerDeathEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onDeath(event));
	}

	public void onItemDrop(PlayerDropItemEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onItemDrop", participants,
				new Class[]{PlayerDropItemEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onItemDrop(event));
	}

	public void onCraftItem(CraftItemEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onItemCraft", participants,
				new Class[]{CraftItemEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onItemCraft(event));
	}

	public void onFish(PlayerFishEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onFish", participants,
				new Class[]{PlayerFishEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onFish(event));
	}

	public void onItemDamage(PlayerItemDamageEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onItemDamage", participants,
				new Class[]{PlayerItemDamageEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onItemDamage(event));
	}

	public void onMove(PlayerMoveEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onMove", participants,
				new Class[]{PlayerMoveEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onMove(event));
	}

	public void onTeleport(PlayerTeleportEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onTeleport", participants,
				new Class[]{PlayerTeleportEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onTeleport(event));
	}

	public void onSneak(PlayerToggleSneakEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onSneak", participants,
				new Class[]{PlayerToggleSneakEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onSneak(event));
	}

	public void onInventoryClick(InventoryClickEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onInventoryClick", participants,
				new Class[]{InventoryClickEvent.class}, event);
	}

	public void onExpBottle(ExpBottleEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onExpBottle", participants,
				new Class[]{ExpBottleEvent.class}, event);
	}

	public void onPlayerExpChange(PlayerExpChangeEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onPlayerExpChange", participants,
				new Class[]{PlayerExpChangeEvent.class}, event);
	}

	public void onEntityTargetLivingEntity(EntityTargetLivingEntityEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onEntityTargetLivingEntity", participants,
				new Class[]{EntityTargetLivingEntityEvent.class}, event);
	}

	public void halt() {
		release();
		Player player = getPlayer();
		if (player == null) {
			return;
		}
		Location location = player.getLocation();
		ArmorStand armorStand = (ArmorStand) location.getWorld().spawnEntity(location.subtract(0, 0.98, 0), EntityType.ARMOR_STAND);
		armorStand.setVisible(false);
		armorStand.setGravity(false);
		armorStand.addPassenger(player);
		this.playerHalter = armorStand;
		this.halted = true;
	}

	public void release() {
		if (!halted || playerHalter == null) {
			return;
		}
		Player player = getPlayer();
		if (player == null) {
			return;
		}
		Entity vehicle = player.getVehicle();
		if (vehicle == null) {
			return;
		}
		if (!playerHalter.equals(vehicle)) {
			return;
		}
		player.eject();
		vehicle.remove();
		playerHalter = null;
		halted = false;
	}

	@Override
	public String toString() {
		Player player = getPlayer();
		return "[HGPlayer{" + (player == null ? getEntityId() : player.getName()) + "}]";
	}

	@Override
	public boolean equals(Object other) {
		return super.equals(other);
	}

}
