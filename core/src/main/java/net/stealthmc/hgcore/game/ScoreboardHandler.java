package net.stealthmc.hgcore.game;

import com.google.common.collect.Maps;
import lombok.experimental.UtilityClass;
import net.stealthmc.hgcommon.bukkit.scoreboard.PlayerScoreboard;
import net.stealthmc.hgcore.HGBukkitMain;
import net.stealthmc.hgcore.config.Messages;
import net.stealthmc.hgcore.event.PlayerScoreboardRegisteredEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.UUID;

@UtilityClass
public class ScoreboardHandler {

	private final Messages messages = HGBukkitMain.getInstance().getMessages();
	private Map<UUID, PlayerScoreboard> playerScoreboards = Maps.newHashMap();

	public boolean isRegistered(Player player) {
		return playerScoreboards.containsKey(player.getUniqueId());
	}

	public PlayerScoreboard getPlayerScoreboard(Player player) {
		return playerScoreboards.compute(player.getUniqueId(), (uuid, playerScoreboard) -> {
			if (playerScoreboard == null) {
				playerScoreboard = new PlayerScoreboard(messages.getScoreboard_title(), player);
				Bukkit.getPluginManager().callEvent(new PlayerScoreboardRegisteredEvent(player, playerScoreboard));
			}
			return playerScoreboard;
		});
	}

	public void unregisterPlayer(Player player) {
		playerScoreboards.compute(player.getUniqueId(), (uuid, playerScoreboard) -> {
			if (playerScoreboard == null) {
				return null;
			}
			playerScoreboard.dispose();
			return null;
		});
	}

}
