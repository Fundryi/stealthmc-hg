package net.stealthmc.hgcore.service.predefined;

import net.stealthmc.hgcore.service.HGService;
import net.stealthmc.hgcore.service.helper.ServiceValueFormatter;

import java.util.Map;

public interface ServiceProvider extends HGService<Map<String, HGService>> {

	Map<String, HGService> get();

	@Override
	default ServiceValueFormatter<Map<String, HGService>> getServiceValueFormatter() {
		return new ServiceValueFormatter.ServiceProviderFormatter(this);
	}
}
