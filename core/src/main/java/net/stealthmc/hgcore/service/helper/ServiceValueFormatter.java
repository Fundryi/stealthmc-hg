package net.stealthmc.hgcore.service.helper;

import lombok.Getter;
import net.stealthmc.hgcommon.util.CommandUtils;
import net.stealthmc.hgcore.service.HGService;

import java.util.Map;

public abstract class ServiceValueFormatter<T> {

	@Getter
	private HGService<T> hgService;

	public ServiceValueFormatter(HGService<T> hgService) {
		this.hgService = hgService;
	}

	public abstract String getFormattedValue();

	public static class BooleanFormatter extends ServiceValueFormatter<Boolean> {

		public BooleanFormatter(HGService<Boolean> hgService) {
			super(hgService);
		}

		@Override
		public String getFormattedValue() {
			return CommandUtils.formatBoolean(getHgService().get());
		}

	}

	public static class ServiceProviderFormatter extends ServiceValueFormatter<Map<String, HGService>> {

		public ServiceProviderFormatter(HGService<Map<String, HGService>> hgService) {
			super(hgService);
		}

		@Override
		public String getFormattedValue() {
			return getHgService().get().size() + " sub services";
		}
	}

	public static class IntegerFormatter extends ServiceValueFormatter<Integer> {

		public IntegerFormatter(HGService<Integer> hgService) {
			super(hgService);
		}

		@Override
		public String getFormattedValue() {
			return getHgService().get().toString();
		}
	}

}
