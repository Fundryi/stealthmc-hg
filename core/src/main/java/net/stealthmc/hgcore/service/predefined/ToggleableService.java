package net.stealthmc.hgcore.service.predefined;

import net.stealthmc.hgcore.service.HGMutableService;
import net.stealthmc.hgcore.service.helper.MutableServiceInputValidator;
import net.stealthmc.hgcore.service.helper.ServiceValueFormatter;

public interface ToggleableService extends HGMutableService<Boolean> {

	Boolean get();

	void set(Boolean bool);

	@Override
	default MutableServiceInputValidator<Boolean> getInputValidator() {
		return new MutableServiceInputValidator.BooleanValidator(this);
	}

	@Override
	default ServiceValueFormatter<Boolean> getServiceValueFormatter() {
		return new ServiceValueFormatter.BooleanFormatter(this);
	}
}
