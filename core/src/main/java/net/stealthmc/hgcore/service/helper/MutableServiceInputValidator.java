package net.stealthmc.hgcore.service.helper;

import lombok.Getter;
import lombok.Setter;
import net.stealthmc.hgcommon.util.CommandUtils;
import net.stealthmc.hgcore.service.HGMutableService;

public abstract class MutableServiceInputValidator<T> {

	@Getter
	@Setter
	private String errorMessage = "ok";

	@Getter
	private HGMutableService<T> hgMutableService;

	public MutableServiceInputValidator(HGMutableService<T> hgMutableService) {
		this.hgMutableService = hgMutableService;
	}

	public abstract boolean acceptInput(String input);

	public static class BooleanValidator extends MutableServiceInputValidator<Boolean> {

		public BooleanValidator(HGMutableService<Boolean> hgMutableService) {
			super(hgMutableService);
		}

		@Override
		public boolean acceptInput(String input) {
			Boolean bool = CommandUtils.getBoolean(input);
			if (bool == null) {
				setErrorMessage("Invalid input for boolean value!");
				return false;
			}
			getHgMutableService().set(bool);
			return true;
		}

	}

	public static class IntegerValidator extends MutableServiceInputValidator<Integer> {

		public IntegerValidator(HGMutableService<Integer> hgMutableService) {
			super(hgMutableService);
		}

		@Override
		public boolean acceptInput(String input) {
			Integer integer = CommandUtils.getInteger(input);
			if (integer == null) {
				setErrorMessage("Invalid input for integer value!");
				return false;
			}
			getHgMutableService().set(integer);
			return true;
		}
	}

}
