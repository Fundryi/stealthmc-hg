package net.stealthmc.hgcore.service;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import lombok.experimental.UtilityClass;
import net.md_5.bungee.api.chat.ClickEvent;
import net.stealthmc.hgcommon.util.Menu;
import net.stealthmc.hgcore.service.helper.ServiceValueFormatter;
import net.stealthmc.hgcore.service.predefined.ServiceProvider;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@UtilityClass
public class ServiceHandler {

	private Map<String, HGService> registeredServices = new HashMap<>();

	public boolean registerService(String key, HGService hgService) {
		Preconditions.checkArgument(!key.contains("."), "Service key cannot contain \".\"!");
		return registeredServices.putIfAbsent(key, hgService) == null;
	}

	public boolean unregisterService(String key) {
		return registeredServices.remove(key) != null;
	}

	public HGService getService(String key) {
		return getService(key, registeredServices);
	}

	private HGService getService(String key, Map<String, HGService> serviceMap) {
		Preconditions.checkArgument(!key.contains("."), "Service key cannot contain \".\"!");
		return serviceMap.get(key);
	}

	public HGService getSubService(String key) {
		return getSubService(key, registeredServices);
	}

	private HGService getSubService(String key, Map<String, HGService> serviceMap) {
		Map<String, HGService> currentSubs = serviceMap;
		int dotIndex;
		while ((dotIndex = key.indexOf('.')) > 0) {
			String current = key.substring(0, dotIndex);
			key = key.substring(dotIndex + 1);
			currentSubs = getSubServices(currentSubs.get(current));
		}
		return currentSubs.get(key);
	}

	public Map<String, HGService> getSubServices(HGService hgService) {
		if (!(hgService instanceof ServiceProvider)) {
			return Collections.emptyMap();
		}
		return ImmutableMap.copyOf(((ServiceProvider) hgService).get());
	}

	public Map<String, HGService> getRegisteredServices() {
		return ImmutableMap.copyOf(registeredServices);
	}

	public List<Menu> getRegisteredServicesPrintable() {
		List<Menu> menuList = new ArrayList<>();
		for (Map.Entry<String, HGService> entry : getRegisteredServices().entrySet()) {
			menuList.add(getSubServicesPrintable(entry.getKey(), entry.getKey(), entry.getValue()));
		}
		return menuList;
	}

	public Menu getSubServicesPrintable(String key, String currentKey, HGService hgService) {
		Map<String, HGService> subServices = getSubServices(hgService);

		if (subServices.isEmpty()) {
			ServiceValueFormatter formatter = hgService.getServiceValueFormatter();
			return new Menu(ChatColor.GOLD + currentKey + ": " + formatter.getFormattedValue(),
					ClickEvent.Action.SUGGEST_COMMAND, "/hgsettings " + key + " ");
		}

		Menu menu = new Menu(ChatColor.GOLD + currentKey);
		for (Map.Entry<String, HGService> entry : subServices.entrySet()) {
			menu.addSub(getSubServicesPrintable(key + "." + entry.getKey(), entry.getKey(), entry.getValue()));
		}

		return menu;
	}

}
