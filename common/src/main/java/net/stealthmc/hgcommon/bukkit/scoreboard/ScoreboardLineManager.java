package net.stealthmc.hgcommon.bukkit.scoreboard;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.AccessLevel;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Team;

import java.util.Map;

public class ScoreboardLineManager {

	@Getter(AccessLevel.PACKAGE)
	private final PlayerScoreboard scoreboard;

	private final Map<String, Team> registeredTeams = Maps.newHashMap();
	private final Map<String, Integer> teamIndeces = Maps.newHashMap();

	private Map<Integer, ScoreboardLine> scoreboardLines = Maps.newHashMap();

	public ScoreboardLineManager(PlayerScoreboard scoreboard) {
		this.scoreboard = scoreboard;
	}

	private Team registerNewTeam() {
		int teams = registeredTeams.size();
		String teamName = "bhk_sb" + teams;
		Team team = scoreboard.getScoreboard().registerNewTeam(teamName);
		registeredTeams.put(teamName, team);
		teamIndeces.put(teamName, teams);
		return team;
	}

	String getTeamEntry(Team team) {
		return getEmptyLine(team) + ChatColor.RESET;
	}

	String getEmptyLine(Team team) {
		if (!registeredTeams.containsKey(team.getName())) {
			return "";
		}
		StringBuilder line = new StringBuilder();
		int i = teamIndeces.get(team.getName());
		while (i > 0) {
			line.append(ChatColor.values()[teamIndeces.get(team.getName()) % ChatColor.values().length]);
			i -= ChatColor.values().length;
		}
		return line.toString();
	}

	ScoreboardLine getLine(int score) {
		return scoreboardLines.compute(score, (integer, scoreboardLine) -> {
			if (scoreboardLine == null) {
				Team team = registerNewTeam();
				String entry = getTeamEntry(team);
				team.addEntry(entry);
				scoreboardLine = new ScoreboardLine(this, team, score);
				Objective objective = scoreboard.getObjective();
				objective.getScore(entry).setScore(score);
			}
			return scoreboardLine;
		});
	}

	void disposeLine(ScoreboardLine line) {
		if (!line.isDisposed()) {
			line.dispose();
		}
		int score = line.getScore();
		scoreboardLines.remove(score);
	}

	boolean existsLine(int score) {
		return scoreboardLines.containsKey(score);
	}

	public void incrementScoreAbove(int threshold) {
		Map<Integer, ScoreboardLine> result = Maps.newHashMap();
		scoreboardLines.forEach((score, scoreboardLine) -> {
			if (score <= threshold) {
				result.put(score, scoreboardLine);
			} else {
				result.put(score + 1, scoreboardLine);
				setUnsafeLineScore(scoreboardLine, score + 1);
			}
		});
		scoreboardLines = result;
	}

	public void decrementScoreBelow(int threshold) {
		Map<Integer, ScoreboardLine> result = Maps.newHashMap();
		scoreboardLines.forEach((score, scoreboardLine) -> {
			if (score >= threshold) {
				result.put(score, scoreboardLine);
			} else {
				result.put(score - 1, scoreboardLine);
				setUnsafeLineScore(scoreboardLine, score - 1);
			}
		});
		scoreboardLines = result;
	}

	public void incrementScoreBelow(int threshold) {
		Map<Integer, ScoreboardLine> result = Maps.newHashMap();
		scoreboardLines.forEach((score, scoreboardLine) -> {
			if (score >= threshold) {
				result.put(score, scoreboardLine);
			} else {
				result.put(score + 1, scoreboardLine);
				setUnsafeLineScore(scoreboardLine, score + 1);
			}
		});
		scoreboardLines = result;
	}

	public void decrementScoreAbove(int threshold) {
		Map<Integer, ScoreboardLine> result = Maps.newHashMap();
		scoreboardLines.forEach((score, scoreboardLine) -> {
			if (score <= threshold) {
				result.put(score, scoreboardLine);
			} else {
				result.put(score - 1, scoreboardLine);
				setUnsafeLineScore(scoreboardLine, score - 1);
			}
		});
		scoreboardLines = result;
	}

	public void trimFromBelow() {
		ScoreboardLine line = scoreboardLines.values().stream().findAny().orElse(null);
		if (line == null) {
			return;
		}
		line = line.belowBlock();
		while (line.isEmpty()) {
			line.dispose();
			line = line.getAbove();
		}
		line.dispose();
	}

	private void setUnsafeLineScore(ScoreboardLine line, int score) {
		Objective objective = scoreboard.getObjective();
		Team team = line.getTeam();
		String entry = getTeamEntry(team);
		objective.getScore(entry).setScore(score);
		line.setScore(score);
	}

	public void dispose() {
		Lists.newArrayList(scoreboardLines.values()).forEach(ScoreboardLine::dispose);
	}

}
