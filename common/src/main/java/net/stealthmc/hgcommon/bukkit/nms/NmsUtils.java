package net.stealthmc.hgcommon.bukkit.nms;

import lombok.experimental.UtilityClass;
import org.bukkit.Bukkit;

@UtilityClass
public class NmsUtils {

	private static final String VERSION;
	private static final String NMS_NAMESPACE = "net.minecraft.server";
	private static final String CRAFTBUKKIT_NAMESPACE = "org.bukkit.craftbukkit";

	static {
		String path = Bukkit.getServer().getClass().getPackage().getName();
		VERSION = path.substring(path.lastIndexOf('.') + 1);
	}

	public Class<?> getNMSClass(String name) throws ClassNotFoundException {
		return Class.forName(NMS_NAMESPACE + "." + VERSION + "." + name);
	}

	public Class<?> getCraftBukkitClass(String name) throws ClassNotFoundException {
		return Class.forName(CRAFTBUKKIT_NAMESPACE + "." + VERSION + "." + name);
	}

}
