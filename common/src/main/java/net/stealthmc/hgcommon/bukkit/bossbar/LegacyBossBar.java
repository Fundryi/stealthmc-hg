package net.stealthmc.hgcommon.bukkit.bossbar;

import lombok.Getter;
import net.stealthmc.hgcommon.bukkit.nms.NmsEntityUtils;
import net.stealthmc.hgcommon.bukkit.nms.NmsPlayerUtils;
import net.stealthmc.hgcommon.bukkit.nms.NmsWorldUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import java.util.UUID;
import java.util.logging.Level;

public class LegacyBossBar implements Listener {

	@Getter
	private UUID playerId;

	private Object entityEnderDragon;

	private BukkitTask locationUpdater;

	@Getter
	private boolean hidden = false;

	private String text;

	public LegacyBossBar(JavaPlugin plugin, Player player, String text) {
		Bukkit.getPluginManager().registerEvents(this, plugin);

		this.playerId = player.getUniqueId();

		Object nmsWorld = NmsWorldUtils.getNMSWorld(player.getWorld());

		if (nmsWorld == null) {
			plugin.getLogger().log(Level.SEVERE, "Could not create a LegacyBossBar for " + player.getName());
			return;
		}

		entityEnderDragon = NmsEntityUtils.newVirtualEnderdragon(nmsWorld);
		updateEnderDragonLocation();
		setText(text);
		spawnForPlayer();
		NmsEntityUtils.setCustomNameVisible(entityEnderDragon, true);
		NmsEntityUtils.setInvisible(entityEnderDragon, true);
		NmsEntityUtils.setSilent(entityEnderDragon, true);
		NmsPlayerUtils.updateEntityMetadata(player, entityEnderDragon);

		locationUpdater = Bukkit.getScheduler().runTaskTimer(plugin, this::updateEnderDragonLocation, 10L, 10L);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onJoin(PlayerJoinEvent event) {
		if (hidden) {
			return;
		}
		Player player = event.getPlayer();
		if (!playerId.equals(player.getUniqueId())) {
			return;
		}
		updateEnderDragonLocation();
		spawnForPlayer();
	}

	public void setText(String text) {
		if (entityEnderDragon == null) {
			return;
		}
		this.text = text;
		NmsEntityUtils.setCustomName(entityEnderDragon, text);
		NmsEntityUtils.setCustomNameVisible(entityEnderDragon, true);
		NmsEntityUtils.setSilent(entityEnderDragon, true);
		NmsEntityUtils.setInvisible(entityEnderDragon, true);
		if (hidden) {
			return;
		}
		Player player = Bukkit.getPlayer(playerId);
		if (player == null) {
			return;
		}
		NmsPlayerUtils.updateEntityMetadata(player, entityEnderDragon);
	}

	private void spawnForPlayer() {
		Player player = Bukkit.getPlayer(playerId);
		if (player == null) {
			return;
		}
		NmsPlayerUtils.spawnVirtualEntity(player, entityEnderDragon);
	}

	private void updateEnderDragonLocation() {
		if (entityEnderDragon == null) {
			return;
		}
		Player player = Bukkit.getPlayer(playerId);
		if (player == null) {
			return;
		}
		Location location = player.getLocation().add(0, 10, 0);
		NmsEntityUtils.setLocation(entityEnderDragon, location);
		if (hidden) {
			return;
		}
		NmsPlayerUtils.updateVirtualEntityLocation(player, entityEnderDragon);
	}

	public void hide() {
		if (hidden) {
			return;
		}
		hidden = true;
		Player player = Bukkit.getPlayer(playerId);
		if (player == null) {
			return;
		}
		NmsPlayerUtils.despawnVirtualEntities(player, entityEnderDragon);
	}

	public void show() {
		if (!hidden) {
			return;
		}
		hidden = false;
		Player player = Bukkit.getPlayer(playerId);
		if (player == null) {
			return;
		}
		Object nmsWorld = NmsWorldUtils.getNMSWorld(player.getWorld());
		if (nmsWorld == null) {
			return;
		}
		entityEnderDragon = NmsEntityUtils.newVirtualEnderdragon(nmsWorld);
		setText(text);
		spawnForPlayer();
	}

	public void destroy() {
		hide();
		locationUpdater.cancel();
		locationUpdater = null;
		playerId = null;
		entityEnderDragon = null;
	}

}
