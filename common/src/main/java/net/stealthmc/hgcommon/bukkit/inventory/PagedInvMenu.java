package net.stealthmc.hgcommon.bukkit.inventory;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.Getter;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.util.TerConsumer;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class PagedInvMenu {

	@Getter
	private final int rowCount;

	@Getter
	private final String title;

	@Getter
	private final TerConsumer<Cancellable, ItemStack, InventoryAction> defaultClickHandler;

	@Getter
	private final Runnable closeHandler;

	private final Runnable backHandler;
	private final int entriesPerPage;
	private final List<PagedMenuEntry> menuEntries = Lists.newArrayList();
	private final PagedMenuEntry[] navigationEntries = new PagedMenuEntry[7];
	private Comparator<ItemStack> sortingComparator = (i1, i2) -> 0;
	private Player player;

	public PagedInvMenu(int rowCount, String title, Player player) {
		this(rowCount, title, null, null, null, player);
	}

	public PagedInvMenu(int rowCount, @Nonnull String title, @Nullable TerConsumer<Cancellable, ItemStack, InventoryAction> defaultClickHandler,
						@Nullable Runnable closeHandler, @Nullable Runnable backHandler, Player player) {
		// Da die unterste Zeile für die Navigation reserviert ist
		Preconditions.checkArgument(rowCount >= 2, "rowCount must be >= 2");

		this.rowCount = rowCount;
		this.title = title;
		this.defaultClickHandler = defaultClickHandler;
		this.closeHandler = closeHandler;
		this.backHandler = backHandler;
		this.player = player;

		entriesPerPage = (rowCount - 1) * 9;
	}

	public void addMenuEntry(@Nonnull ItemStack specialItem, @Nullable TerConsumer<Cancellable, ItemStack, InventoryAction> clickHandler) {
		menuEntries.add(new PagedMenuEntry(specialItem, clickHandler));
	}

	public void setSortingComparator(@Nullable Comparator<ItemStack> sortingComparator) {
		if (sortingComparator == null) {
			this.sortingComparator = (i1, i2) -> 0;
		} else {
			this.sortingComparator = sortingComparator;
		}
	}

	public void setNavigationEntry(int column, @Nonnull ItemStack displayItemStack, @Nullable TerConsumer<Cancellable, ItemStack, InventoryAction> clickHandler) {
		Preconditions.checkArgument(column >= 2 && column < 9, "columns must be in [2, 9]");
		navigationEntries[column - 2] = new PagedMenuEntry(displayItemStack, clickHandler);
	}

	@SuppressWarnings("deprecation") public @Nonnull
	InventoryMenu getInvMenuForPage(int page) {
		InventoryMenu menu = new InventoryMenu(title, rowCount, defaultClickHandler, closeHandler);
		menu.fillBottomBar(this.backHandler);

		AtomicInteger index = new AtomicInteger();
		menuEntries.stream().sorted(Comparator.comparing(PagedMenuEntry::getDisplayItemStack, sortingComparator)).skip(page * entriesPerPage)
				.limit(entriesPerPage).forEachOrdered(
				menuEntry -> menu.addItemAndClickHandler(menuEntry.getDisplayItemStack(), index.getAndIncrement(), menuEntry.getClickHandler()));

		AtomicInteger navigationIndex = new AtomicInteger(2);
		Arrays.stream(navigationEntries).filter(Objects::nonNull).forEachOrdered(menuEntry -> menu
				.addItemAndClickHandler(menuEntry.getDisplayItemStack(), this.rowCount - 1, navigationIndex.getAndIncrement(),
						menuEntry.getClickHandler()));

		if (page > 0) {

			ItemStack previousPage = new ItemStack(Material.PLAYER_HEAD);
			SkullMeta skullMeta = (SkullMeta) previousPage.getItemMeta();
			skullMeta.setDisplayName(CC.aqua + "Previous Page");
			skullMeta.setOwner("MHF_ArrowLeft"); //DEPRECATED
			previousPage.setItemMeta(skullMeta);

			menu.addItemAndClickHandler(previousPage, this.rowCount - 1, 0,
					(cancellable, itemStack, inventoryAction) -> getInvMenuForPage(page - 1).open(player));
		}

		if ((menuEntries.size() - 1) / entriesPerPage > page) {

			ItemStack nextPage = new ItemStack(Material.PLAYER_HEAD);
			SkullMeta skullMeta = (SkullMeta) nextPage.getItemMeta();
			skullMeta.setDisplayName(CC.aqua + "Next Page");
			skullMeta.setOwner("MHF_ArrowRight"); //DEPRECATED
			nextPage.setItemMeta(skullMeta);

			menu.addItemAndClickHandler(nextPage, this.rowCount - 1, 8,
					(cancellable, itemStack, inventoryAction) -> getInvMenuForPage(page + 1).open(player));
		}

		return menu;
	}

	public void openInventory(@Nonnull Player player) {
		openInventory(player, 0);
	}

	public void openInventory(@Nonnull Player player, int page) {
		getInvMenuForPage(page).open(player);
	}

	@Data
	private static class PagedMenuEntry {
		private final @Nonnull
		ItemStack displayItemStack;
		private final @Nullable
		TerConsumer<Cancellable, ItemStack, InventoryAction> clickHandler;
	}

}
