package net.stealthmc.hgcommon.bukkit.nms;

import lombok.Getter;
import lombok.experimental.UtilityClass;
import org.bukkit.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@UtilityClass
public class NmsWorldUtils {

	private Class<?> craftWorldClass;
	private Method craftWorldGetHandleMethod;

	@Getter
	private Class<?> nmsWorldClass;

	static {
		try {
			craftWorldClass = NmsUtils.getCraftBukkitClass("CraftWorld");
			nmsWorldClass = NmsUtils.getNMSClass("World");

			craftWorldGetHandleMethod = craftWorldClass.getMethod("getHandle");
		} catch (ClassNotFoundException | NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	public @Nullable
	Object getNMSWorld(@Nonnull World world) {
		try {
			return craftWorldGetHandleMethod.invoke(world);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

}
