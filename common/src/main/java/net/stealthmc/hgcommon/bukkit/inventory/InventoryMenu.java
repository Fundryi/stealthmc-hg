package net.stealthmc.hgcommon.bukkit.inventory;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.Getter;
import lombok.Setter;
import net.stealthmc.hgcommon.util.TerConsumer;
import net.stealthmc.hgcommon.util.TerFunction;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.IntStream;

public class InventoryMenu implements Listener {

	static final Set<InventoryAction> LEGIT_ACTIONS = ImmutableSet.of(
			InventoryAction.PICKUP_ALL,
			InventoryAction.PICKUP_HALF,
			InventoryAction.PICKUP_ONE,
			InventoryAction.PICKUP_SOME,
			InventoryAction.CLONE_STACK,
			InventoryAction.PLACE_ALL,
			InventoryAction.PLACE_ONE,
			InventoryAction.MOVE_TO_OTHER_INVENTORY
	);
	private static JavaPlugin plugin;
	private final int rows;
	private Map<Integer, TerConsumer<Cancellable, ItemStack, InventoryAction>> clickHandlers = Maps.newHashMap();
	private Set<Integer> modifiableSlots = Sets.newHashSet();

	@Getter
	private Inventory inventory;

	@Getter
	@Setter
	private boolean locked = false;

	@Getter
	private TerConsumer<Cancellable, ItemStack, InventoryAction> defaultClickHandler;

	@Getter
	private Runnable backHandler;

	@Getter
	@Setter
	private Runnable closeHandler;

	@Getter
	@Setter
	private Consumer<ItemStack> modifiedHandler;

	@Getter
	@Setter
	private TerFunction<Integer, ItemStack, InventoryAction, Boolean> playerInventoryClickHandler;

	@Getter
	private Player player;

	public InventoryMenu(@Nonnull String title, final int rows, @Nullable TerConsumer<Cancellable, ItemStack, InventoryAction> defaultClickHandler,
						 @Nullable Runnable backHandler) {
		Preconditions.checkNotNull(title, "title");
		Preconditions.checkArgument(rows <= 6, "Only 6 rows are possible!");

		if (defaultClickHandler == null) {
			defaultClickHandler = (player, itemStack, action) -> this.unlock();
		}

		this.rows = rows;

		this.defaultClickHandler = defaultClickHandler;
		this.backHandler = backHandler;

		Bukkit.getPluginManager().registerEvents(this, InventoryMenu.plugin);

		inventory = Bukkit.createInventory(null, 9 * rows, title);

		for (int i = 0; i < inventory.getSize(); i++) {
			inventory.setItem(i, ItemStackUtils.INVENTORY_DEFAULT_ITEM);
		}
	}

	public static void init(JavaPlugin plugin) {
		InventoryMenu.plugin = plugin;
	}

	@EventHandler
	public void onDrag(InventoryDragEvent event) {
		if (event.getInventory() == null) {
			return;
		}
		if (!event.getInventory().getViewers().contains(event.getWhoClicked())) {
			return;
		}
		if (event.getInventory().equals(inventory)) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void onClick(InventoryClickEvent event) {
		if (event.getClickedInventory() == null) {
			return;
		}
		if (!event.getWhoClicked().equals(player)) {
			return;
		}
		if (player.getInventory().equals(event.getClickedInventory())) {
			boolean cancel = true;
			if (playerInventoryClickHandler != null) {
				cancel = playerInventoryClickHandler.apply(event.getSlot(), event.getCurrentItem(), event.getAction());
			}
			event.setCancelled(cancel);
			return;
		}
		if (!event.getClickedInventory().equals(inventory)) {
			return;
		}
		if (!LEGIT_ACTIONS.contains(event.getAction())) {
			event.setCancelled(true); //nicht abbrechen.
			unlock();
		}
		if (locked) {
			event.setCancelled(true);
			return;
		}
		if (!modifiableSlots.contains(event.getSlot())) {
			event.setCancelled(true);
		} else if (modifiedHandler != null) {
			modifiedHandler.accept(event.getCurrentItem());
		}
		clickHandlers.getOrDefault(event.getSlot(), defaultClickHandler)
				.accept(event, event.getCurrentItem(), event.getAction());
		locked = true;
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onClose(InventoryCloseEvent event) {
		if (!event.getInventory().equals(inventory)) {
			return;
		}
		HandlerList.unregisterAll(this);
		if (closeHandler != null) {
			closeHandler.run();
		}
		player = null;
	}

	public void open(Player player) {
		if (this.player != null) {
			throw new RuntimeException("InventoryMenu is already opend by/for other Player(s)");
		}
		this.player = player;
		player.openInventory(inventory);
	}

	public void fillBorder() {
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < rows; j++) {
				if (j == 0 || j == rows - 1 || i == 0 || i == 8) {
					addItemAndClickHandler(ItemStackUtils.INVENTORY_DARK_ITEM, j, i, defaultClickHandler);
				}
			}
		}
		if (backHandler != null) {
			addItemAndClickHandler(ItemStackUtils.INVENTORY_BACK_ITEM, rows - 1, 4, (p, i, a) -> backHandler.run());
		} else {
			addItemAndClickHandler(ItemStackUtils.INVENTORY_DARK_ITEM, rows - 1, 4, defaultClickHandler);
		}
	}

	public void fill(ItemStack itemStack) {
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < rows; j++) {
				addItemAndClickHandler(itemStack, j, i, null);
			}
		}
	}

	public void fillBottomBar(Runnable backHandler) {

		IntStream.range(0, 9)
				.forEach(column -> addItemAndClickHandler(ItemStackUtils.INVENTORY_DARK_ITEM, this.rows - 1, column, null));

		addItemAndClickHandler(ItemStackUtils.INVENTORY_BACK_ITEM, this.rows - 1, 4,
				backHandler == null ? (cancellable, itemStack, inventoryAction) -> this.player.closeInventory() :
						(cancellable, itemStack, inventoryAction) -> backHandler.run());

	}

	public void unlock() {
		Bukkit.getScheduler().runTaskLater(InventoryMenu.plugin, () -> this.setLocked(false), 1L);
	}

	public void addModifiableSlot(int row, int column) {
		modifiableSlots.add(row * 9 + column);
	}

	public void addModifiableSlot(int indexedSlot) {
		modifiableSlots.add(indexedSlot);
	}

	public void addItemAndClickHandler(@Nullable ItemStack itemStack, int row, int column, @Nullable TerConsumer<Cancellable, ItemStack, InventoryAction> clickHandler) {
		addItemAndClickHandler(itemStack, 9 * row + column, clickHandler);
	}

	public void addItemAndClickHandler(@Nullable ItemStack itemStack, int indexedSlot, @Nullable TerConsumer<Cancellable, ItemStack, InventoryAction> clickHandler) {
		inventory.setItem(indexedSlot, itemStack);

		if (clickHandler != null) {
			clickHandlers.put(indexedSlot, clickHandler);
		}
	}

}
