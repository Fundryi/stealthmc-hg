package net.stealthmc.hgcommon.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class MathUtils {

	public double clamp(double min, double max, double val) {
		return Math.max(min, Math.min(max, val));
	}

}
