package net.stealthmc.hgcommon.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class StringUtils {

	public String normalizeEnumName(Enum<?> e) {
		StringBuilder name = new StringBuilder(e.name());
		for (int index = 0; index < name.length(); index++) {
			char c = name.charAt(index);
			int indexPrev = index - 1;
			if (indexPrev < 0) {
				continue;
			}
			if (name.charAt(indexPrev) == ' ') {
				name.setCharAt(index, Character.toUpperCase(c));
				continue;
			}
			if (c == '_') {
				name.setCharAt(index, ' ');
				continue;
			}
			name.setCharAt(index, Character.toLowerCase(c));
		}
		return name.toString();
	}

	public String firstLetterUpper(String in) {
		StringBuilder sb = new StringBuilder();
		boolean start = true;
		for (char c : in.toCharArray()) {
			if (start) {
				sb.append(Character.toUpperCase(c));
				start = false;
			} else {
				sb.append(Character.toLowerCase(c));
			}
		}
		return sb.toString();
	}

}
