package net.stealthmc.hgcommon.util;

import lombok.experimental.UtilityClass;

import java.io.File;
import java.util.Arrays;

@UtilityClass
public class FileUtils {

	public boolean deleteRecursively(File file, File... childFilesToSkip) {
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			if (files != null) {
				for (File child : files) {
					if (Arrays.asList(childFilesToSkip).contains(child)) continue;

					if (!deleteRecursively(child)) {
						return false;
					}
				}
			}
		}

		return file.delete();
	}

}
