package net.stealthmc.hgcommon.util;

import org.bukkit.ChatColor;

import java.math.BigDecimal;
import java.math.MathContext;

public class ProgressBar {

	private BigDecimal requiredProgress;

	private String[] barCharacters;

	public ProgressBar(int requiredProgress) {
		this(requiredProgress, new String[]{ChatColor.GREEN + "|", ChatColor.RED + "|"});
	}

	public ProgressBar(int requiredProgress, String[] barCharacters) {
		requiredProgress = Math.max(1, requiredProgress);
		this.requiredProgress = new BigDecimal(String.valueOf(requiredProgress));
		this.barCharacters = barCharacters;
	}

	public String getBar(int barLength, int progress) {
		BigDecimal currentProgress = new BigDecimal(String.valueOf(progress));

		BigDecimal progressPercentage = currentProgress.divide(requiredProgress, MathContext.DECIMAL32);
		BigDecimal segmentPercentage = BigDecimal.ONE.divide(BigDecimal.valueOf(barLength), MathContext.DECIMAL32);

		StringBuilder sb = new StringBuilder();

		int sl = barCharacters.length - 1;

		outer:
		for (int i = 0; i < barLength; i++) {
			BigDecimal segDisp = progressPercentage.subtract(segmentPercentage.multiply(BigDecimal.valueOf(i)))
					.divide(segmentPercentage, MathContext.DECIMAL32);
			for (int j = sl; j > 0; j--) {
				if (segDisp.compareTo(BigDecimal.valueOf(j / (double) sl)) >= 0) {
					sb.append(barCharacters[j]);
					continue outer;
				}
			}
			sb.append(barCharacters[0]);
		}

		return sb.toString();
	}

}
