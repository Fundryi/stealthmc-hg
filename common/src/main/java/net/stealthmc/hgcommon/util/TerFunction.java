package net.stealthmc.hgcommon.util;

public interface TerFunction<T, U, V, R> {

	R apply(T t, U u, V v);

}
