package net.stealthmc.hgcommon.util;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.util.*;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

@AllArgsConstructor
@Data
public class HandledList<T> implements List<T> {

	@Getter(AccessLevel.PRIVATE)
	private final List<T> toHandle;

	private Function<T, T> addHandler;

	@Override
	public int size() {
		return toHandle.size();
	}

	@Override
	public boolean isEmpty() {
		return toHandle.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return toHandle.contains(o);
	}

	@Override
	public Iterator<T> iterator() {
		return toHandle.iterator();
	}

	@Override
	public Object[] toArray() {
		return toHandle.toArray();
	}

	@Override
	public <T1> T1[] toArray(T1[] a) {
		return toHandle.toArray(a);
	}

	@Override
	public boolean add(T t) {
		if (addHandler != null) {
			t = addHandler.apply(t);
		}
		return toHandle.add(t);
	}

	@Override
	public boolean remove(Object o) {
		return toHandle.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return toHandle.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		if (addHandler != null) {
			c = c.stream().map(addHandler)
					.collect(Collectors.toList());
		}
		return toHandle.addAll(c);
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		if (addHandler != null) {
			c = c.stream().map(addHandler)
					.collect(Collectors.toList());
		}
		return toHandle.addAll(index, c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return toHandle.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return toHandle.retainAll(c);
	}

	@Override
	public void replaceAll(UnaryOperator<T> operator) {
		toHandle.replaceAll(operator);
	}

	@Override
	public void sort(Comparator<? super T> c) {
		toHandle.sort(c);
	}

	@Override
	public void clear() {
		toHandle.clear();
	}

	@Override
	public boolean equals(Object o) {
		return toHandle.equals(o);
	}

	@Override
	public int hashCode() {
		return toHandle.hashCode();
	}

	@Override
	public T get(int index) {
		return toHandle.get(index);
	}

	@Override
	public T set(int index, T element) {
		if (addHandler != null) {
			element = addHandler.apply(element);
		}
		return toHandle.set(index, element);
	}

	@Override
	public void add(int index, T element) {
		toHandle.add(index, element);
	}

	@Override
	public T remove(int index) {
		return toHandle.remove(index);
	}

	@Override
	public int indexOf(Object o) {
		return toHandle.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		return toHandle.lastIndexOf(o);
	}

	@Override
	public ListIterator<T> listIterator() {
		return toHandle.listIterator();
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		return toHandle.listIterator(index);
	}

	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		return toHandle.subList(fromIndex, toIndex);
	}

	@Override
	public Spliterator<T> spliterator() {
		return toHandle.spliterator();
	}
}
