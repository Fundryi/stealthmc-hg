package net.stealthmc.hgcommon.util;

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import lombok.experimental.UtilityClass;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@UtilityClass
public class MapUtils {

	public <K, V> Map<K, V> of(Supplier<Map<K, V>> mapSupplier, Object... objects) {
		Preconditions.checkArgument(objects.length % 2 == 0);
		Map<K, V> result = mapSupplier.get();
		for (int a = 0; a < objects.length; a += 2) {
			result.put((K) objects[a], (V) objects[a + 1]);
		}
		return result;
	}

	public boolean anyListKeyContains(Collection<? extends List<?>> keySet, Object value) {
		return keySet.stream().anyMatch(list -> list.contains(value));
	}

}
