package net.stealthmc.hgcommon.handler;

import java.util.Collection;

public interface IHGMain {

	Collection<? extends GlobalPlayer> getOnlinePlayers();

}
