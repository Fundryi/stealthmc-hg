@echo off
echo COPY Plugins in one place so you can copy/upload them easier.

#xcopy /Y /S "%cd%\common\target\*.jar" "%cd%"
xcopy /Y /S "%cd%\core\target\*.jar" "%cd%"
xcopy /Y /S "%cd%\kits\target\*.jar" "%cd%"

del "%cd%\original-stealthmc-hg-core-*.jar"

exit