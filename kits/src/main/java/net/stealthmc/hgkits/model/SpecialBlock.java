package net.stealthmc.hgkits.model;

import lombok.Data;
import net.stealthmc.hgcommon.bukkit.LocationUtils;
import net.stealthmc.hgcore.game.GameHandler;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;

import java.util.UUID;

@Data
public abstract class SpecialBlock {

	protected double downCheckDistance = 0.1;
	protected double detectionRadiusSquared = 1.125; //player is at least on the very edge
	private Block block;
	private UUID blockOwner;

	public SpecialBlock(Block block, UUID blockOwner) {
		this.block = block;
		this.blockOwner = blockOwner;
	}

	public boolean isInProximity(Player player) {
		if (!GameHandler.getInstance().getCurrentPhase().isEntityDamageEnabled()) {
			return false; //Disarm blocks in lobby and ending phase
		}
		Location location = player.getLocation();
		return isInProximity(location);
	}

	/**
	 * Intended for derivative classes only
	 *
	 * @param location the location
	 * @return true if in Proximity with default implementation
	 */
	protected boolean isInProximity(Location location) {
		if (!location.getWorld().equals(block.getWorld())) {
			return false;
		}
		return location.distanceSquared(LocationUtils.toCenterBlockLocation(block)) <= detectionRadiusSquared;
	}

	public void onLeftClick(Player player) {

	}

	public void onRightClick(Player player) {

	}

	public void onStepOver(Player stepped) {

	}

	public abstract boolean shouldDispose();

	public void onBlockBreak(BlockBreakEvent event) {

	}

}
