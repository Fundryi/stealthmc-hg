package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcore.HGBukkitMain;
import net.stealthmc.hgcore.game.CraftingRecipeHandler;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.tasks.CooldownTitleAnimation;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by petomka | PAID

 */

public class DiggerKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Place your Dragon Eggs to excavate",
			CC.gray + "a large area instantly. You can craft",
			CC.gray + "additional Dragon Eggs."
	);

	private static ShapedRecipe dragonEggRecipe;

	static {
		dragonEggRecipe = new ShapedRecipe(new NamespacedKey(HGBukkitMain.getInstance(), "DiggerEgg"), new ItemStack(Material.DRAGON_EGG));
		dragonEggRecipe.shape(
				"AIA",
				"SIS",
				"ASA"
		);
		dragonEggRecipe.setIngredient('I', Material.IRON_INGOT);
		dragonEggRecipe.setIngredient('S', Material.STONE_SHOVEL);
		CraftingRecipeHandler.registerRecipe(dragonEggRecipe);
		KitsMain.getInstance().getSpecialRecipes().add(dragonEggRecipe);
	}

	private int placements = 0;
	private int cooldownSeconds = 0;

	public DiggerKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.DRAGON_EGG, 0, CC.gold + "Digger", description);
		if (playerToAdapt == null) {
			return;
		}
		getStartingItems().add(new ItemStack(Material.DRAGON_EGG, 15));
		GameHandler.getInstance().getSecondsHandler().add(() -> {
			cooldownSeconds = Math.max(0, cooldownSeconds - 1);
			if (cooldownSeconds == 0) {
				placements = 0;
			}
		});
	}

	@Override
	public void onBlockPlace(BlockPlaceEvent event) {
		if (event.getBlock().getType() != Material.DRAGON_EGG) {
			return;
		}
		if (placements >= 3) {
			event.setCancelled(true);
			KitsMain.sendCooldownMessage(event.getPlayer(), cooldownSeconds);
			CooldownTitleAnimation.play(event.getPlayer(), cooldownSeconds);
			return;
		}
		cooldownSeconds = 30;
		++placements;
		event.getBlock().setType(Material.AIR);
		PlayerUtils.sendMessage(event.getPlayer(), CC.red + "Digger hole will appear in 1.5 seconds, run!");
		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), () -> excavate(event.getBlock()), 30L);
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.getType() == Material.DRAGON_EGG);
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().getType() != Material.DRAGON_EGG) {
			return;
		}
		event.setCancelled(true);
	}

	@Override
	public boolean canCraftSpecialRecipe(Recipe recipe) {
		return recipe.equals(dragonEggRecipe);
	}

	private void excavate(Block origin) {
		int xFrom = origin.getX() - 2, xTo = xFrom + 5,
				yFrom = Math.max(1, origin.getY() - 5), yTo = yFrom + 5,
				zFrom = origin.getZ() - 2, zTo = zFrom + 5;

		World world = origin.getWorld();

		KitsMain.forXYZBlocks(world, xFrom, xTo, yFrom, yTo, zFrom, zTo, (block) -> {
			if (block.getType() == Material.BEDROCK) {
				return;
			}
			block.setType(Material.AIR);
		});
	}

}
