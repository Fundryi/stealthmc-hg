package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/*

Coded by Fundryi

 */

public class SnailKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You have a 25% chance of giving",
			CC.gray + "slowness 2 for 5 seconds to a player."
	);

	private static final int chance = 25;
	private static final int length = 5;
	private static final int multiplier = 0;

	public SnailKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.SOUL_SAND, 0, CC.gold + "Snail", description);
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (event.isCancelled()) {
			return;
		}
		if (!(event.getDamager() instanceof Player)) {
			return;
		}
		if (!(event.getEntity() instanceof LivingEntity)) {
			return;
		}

		LivingEntity entity = (LivingEntity) event.getEntity();
		if (ThreadLocalRandom.current().nextInt(100) <= chance) {
			entity.getWorld().playEffect(entity.getLocation().add(0, 2, 0), Effect.SMOKE, 0);
			entity.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, length * 20, multiplier));
		}
	}

}
