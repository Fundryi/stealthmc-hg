package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.tasks.CooldownTitleAnimation;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class MinerKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Mining is fun for you!",
			CC.gray + "You can Veinmine any kind of Ore!",
			CC.gray + "Your Pickaxe is quite fast...",
			CC.gray + "\"Doctors hate him\"",
			CC.gray + "Apples give you effects and make you healthier."
	);

	private static final int COOLDOWN = 20;
	private static ItemStack ABILITY_ITEM;

	static {
		ABILITY_ITEM = Kit.createItemStack(Material.STONE_PICKAXE, CC.red + "Drill");
		ItemMeta meta = ABILITY_ITEM.getItemMeta();
		meta.addEnchant(Enchantment.DIG_SPEED, 6, true);
		ABILITY_ITEM.setItemMeta(meta);
	}

	private int cooldownSeconds = 0;

	public MinerKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.STONE_PICKAXE, 0, CC.gold + "Miner", description);
		getStartingItems().add(ABILITY_ITEM);
		getStartingItems().add(new ItemStack(Material.APPLE, 3));
		GameHandler.getInstance().getSecondsHandler().add(() -> {
			cooldownSeconds = Math.max(0, cooldownSeconds - 1);
		});
	}

	@Override
	public void onBlockBreak(BlockBreakEvent event) {
		Material getBlock = (event).getBlock().getType();
		if (!(getBlock == Material.COAL_ORE
				|| getBlock == Material.DIAMOND_ORE
				|| getBlock == Material.EMERALD_ORE
				|| getBlock == Material.GOLD_ORE
				|| getBlock == Material.IRON_ORE
				|| getBlock == Material.LAPIS_ORE
				|| getBlock == Material.NETHER_QUARTZ_ORE
				|| getBlock == Material.REDSTONE_ORE)) {
			return;
		}
		Block block = event.getBlock();
		destroySuroundingBlocks(block);
	}

	private void destroySuroundingBlocks(Block block) {
		Material material = block.getType();
		block.breakNaturally();

		for (BlockFace face : BlockFace.values()) {
			if (face == BlockFace.SELF)
				continue;

			Block adjacentBlock = block.getRelative(face);
			if (adjacentBlock.getType() == material)
				destroySuroundingBlocks(adjacentBlock);
		}
	}

	@Override
	public void onInteract(PlayerInteractEvent event) {
		if (!event.getAction().name().contains("RIGHT")) {
			return;
		}
		Player player = event.getPlayer();
		if (event.getItem().getType() != Material.APPLE) {
			return;
		}
		if (cooldownSeconds > 0) {
			event.setCancelled(true);
			KitsMain.sendCooldownMessage(event.getPlayer(), cooldownSeconds);
			CooldownTitleAnimation.play(event.getPlayer(), cooldownSeconds);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
			return;
		}
		event.setCancelled(true);

		event.getItem().setAmount(event.getItem().getAmount() - 1);
		if (event.getItem().getAmount() == 0) {
			player.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
		}
		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);

		player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 20 * 120, 0, false, true));
		player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 20 * 10, 0, false, true));
		player.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, 20 * 5, 4, false, true));

		cooldownSeconds = COOLDOWN;
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

	@Override
	public void onItemDamage(PlayerItemDamageEvent event) {
		if (event.getItem().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}
}
