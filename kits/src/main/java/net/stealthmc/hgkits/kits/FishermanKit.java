package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by petomka | PAID

 */

public class FishermanKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Throw out your hook and teleport",
			CC.gray + "any living being you hit instantly",
			CC.gray + "to your position!"
	);

	private static final ItemStack ABILITY_ITEM = Kit.createItemStack(Material.FISHING_ROD,
			CC.red + "The Hook");

	public FishermanKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.FISHING_ROD, 0, CC.gold + "Fisherman", description);
		getStartingItems().add(ABILITY_ITEM);
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

	@Override
	public void onFish(PlayerFishEvent event) {
		ItemStack inHand = event.getPlayer().getInventory().getItemInMainHand();
		if (!inHand.isSimilar(ABILITY_ITEM)) {
			return;
		}
		if (event.getState() != PlayerFishEvent.State.CAUGHT_ENTITY) {
			return;
		}
		Entity caught = event.getCaught();
		if (caught == null) {
			return;
		}
		caught.teleport(event.getPlayer());

		if (caught instanceof Player) {
			Player caughtPlayer = (Player) caught;

			caughtPlayer.damage(1, event.getPlayer());

			caughtPlayer.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20, 0));
			PlayerUtils.sendMessage(event.getPlayer(), CC.green + "You have " + CC.red + caught.getName() + CC.green + " on your Hook!");
			PlayerUtils.sendMessage(caught, CC.red + event.getPlayer().getName() + CC.green + " has you on his Hook. Look Out!");

			//PlayerUtils.sendAction(event.getPlayer(), CC.green + "You Caught " + CC.red + caughtPlayer.getDisplayName());
			//PlayerUtils.sendMessage(caught, CC.red + "You have been fished by a Fisherman!");
		} else {
			PlayerUtils.sendAction(event.getPlayer(), CC.green + "You Caught a " + CC.red + caught.getType());
		}
	}

	@Override
	public void onItemDamage(PlayerItemDamageEvent event) {
		if (event.getItem().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}
}
