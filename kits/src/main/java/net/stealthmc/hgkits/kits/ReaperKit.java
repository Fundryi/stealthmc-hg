package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/*

Coded by Fundryi

 */

public class ReaperKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You have a 20% chance of giving",
			CC.gray + "Wither Effect 1 for 5 seconds to a player."
	);

	private static final int chance = 20;
	private static final int length = 5;
	private static final int multiplier = 0;
	private static final ItemStack ABILITY_ITEM;

	static {
		ABILITY_ITEM = Kit.createItemStack(Material.WOODEN_HOE, CC.red + "Reaper Hoe");
	}

	public ReaperKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.WOODEN_HOE, 0, CC.gold + "Reaper", description);
		getStartingItems().add(ABILITY_ITEM);
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (event.isCancelled()) {
			return;
		}
		if (!(event.getDamager() instanceof Player)) {
			return;
		}
		if (!(event.getEntity() instanceof LivingEntity)) {
			return;
		}
		ItemStack itemStack = ((Player) event.getDamager()).getInventory().getItemInMainHand();
		if (!itemStack.isSimilar(ABILITY_ITEM)) {
			return;
		}
		LivingEntity entity = (LivingEntity) event.getEntity();
		if (ThreadLocalRandom.current().nextInt(100) <= chance) {
			entity.getWorld().playEffect(entity.getLocation().add(0, 2, 0), Effect.SMOKE, 0);
			entity.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, length * 20, multiplier));
		}
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

	@Override
	public void onItemDamage(PlayerItemDamageEvent event) {
		if (event.getItem().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}
}
