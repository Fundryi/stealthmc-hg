package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class TankKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You take no damage from explosions,",
			CC.gray + "and if your opponents die,",
			CC.gray + "they will explode!",
			"",
			CC.red + CC.thickX + CC.gold + " This kit is WIP! " + CC.red + CC.thickX
	);

	public TankKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.TNT, 0, CC.gold + "Tank", description);
	}

	@Override
	public void onDamage(EntityDamageEvent event) {
		if (!(event.getEntity() instanceof Player)) {
			return;
		}
		if (!(event.getCause() == EntityDamageEvent.DamageCause.ENTITY_EXPLOSION || event.getCause() == EntityDamageEvent.DamageCause.BLOCK_EXPLOSION)) {
			return;
		}
		event.setCancelled(true);
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (!(event.getEntity() instanceof LivingEntity)) {
			return;
		}
		if (!(event.getDamager() instanceof Player)) {
			return;
		}

		LivingEntity entity = (LivingEntity) event.getEntity();

		if (entity.getUniqueId().equals(super.getEntityId())) { //FILTERS KITUSER OUT AS VICTIM
			return;
		}
		if (!(entity.getHealth() - event.getFinalDamage() <= 0)) {
			return;
		}

		if (event.getEntity() instanceof Player) {

			TNTPrimed tnt = entity.getWorld().spawn(entity.getEyeLocation(), TNTPrimed.class);
			tnt.setYield(5f);
			tnt.setFuseTicks(0);

			for (Entity cascadeEntity : tnt.getNearbyEntities(5, 5, 5)) {
				if (cascadeEntity instanceof Player) {

					Player cascadeEntityPlayer = (Player) entity;

					if (KitsMain.getInstance().hasKit(cascadeEntityPlayer, this.getClass())) {
						return;
					}
					if (!(cascadeEntityPlayer.getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.ENTITY_EXPLOSION || cascadeEntityPlayer.getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.BLOCK_EXPLOSION)) {
						return;
					}
					if (!(cascadeEntityPlayer.getHealth() - event.getFinalDamage() <= 0)) {
						return;
					}

					TNTPrimed cascadeEntityPlayerTNT = cascadeEntityPlayer.getWorld().spawn(cascadeEntityPlayer.getEyeLocation(), TNTPrimed.class);
					cascadeEntityPlayerTNT.setYield(4f);
					cascadeEntityPlayerTNT.setFuseTicks(0);
				}
			}
		}
		if (event.getEntity() instanceof LivingEntity) {

			if (event.getEntity() instanceof Player) {
				return;
			}

			TNTPrimed tnt = entity.getWorld().spawn(entity.getEyeLocation(), TNTPrimed.class);
			tnt.setYield(3f);
			tnt.setFuseTicks(0);

			for (Entity cascadeEntity : tnt.getNearbyEntities(5, 5, 5)) {
				if (cascadeEntity instanceof LivingEntity) {

					if (!(entity.getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.ENTITY_EXPLOSION || entity.getLastDamageCause().getCause() == EntityDamageEvent.DamageCause.BLOCK_EXPLOSION)) {
						return;
					}
					if (!(entity.getHealth() - event.getFinalDamage() <= 0)) {
						return;
					}

					TNTPrimed cascadeEntityPlayerTNT = entity.getWorld().spawn(entity.getEyeLocation(), TNTPrimed.class);
					cascadeEntityPlayerTNT.setYield(2f);
					cascadeEntityPlayerTNT.setFuseTicks(0);
				}
			}
		}
	}
}