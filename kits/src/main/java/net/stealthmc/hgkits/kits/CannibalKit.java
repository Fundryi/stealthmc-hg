package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/*

Coded by Fundryi

 */

public class CannibalKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You have a 35% chance of giving",
			CC.gray + "hunger 2 for 5 seconds to a player.",
			CC.gray + "You also restore hunger if you hit them."
	);

	private static final int chance = 35;
	private static final int length = 10;
	private static final int multiplier = 10;
	private static final int addHunger = 3;

	public CannibalKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.ROTTEN_FLESH, 0, CC.gold + "Cannibal", description);
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (event.isCancelled()) {
			return;
		}
		if (!(event.getDamager() instanceof Player)) {
			return;
		}
		if (!(event.getEntity() instanceof LivingEntity)) {
			return;
		}

		LivingEntity entity = (LivingEntity) event.getEntity();
		Player player = (Player) event.getDamager();
		int hunger = player.getFoodLevel();
		if (hunger < 30) {
			hunger += addHunger;
			player.setFoodLevel(hunger);
		}

		if (ThreadLocalRandom.current().nextInt(100) <= chance) {
			entity.getWorld().playEffect(entity.getLocation(), Effect.EXTINGUISH, 3);
			entity.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, length * 20, multiplier));
		}
	}

}
