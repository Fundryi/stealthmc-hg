package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.tasks.CooldownTitleAnimation;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/*

Coded by Fundryi

 */

public class MonkKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Confuse your enemies with your Pooopy Stick!",
			CC.gray + "Damaging a player with your Pooopy Stick will Swap",
			CC.gray + "their Item in Hand with a random on in their inventory",
			"",
			CC.red + CC.thickX + CC.gold + " This kit is WIP! " + CC.red + CC.thickX
	);

	private static final ItemStack ABILITY_ITEM;
	private static final int COOLDOWN = 20;

	static {
		ABILITY_ITEM = Kit.createItemStack(Material.BLAZE_ROD, CC.red + "Pooopy Stick");
	}

	private int cooldownSeconds = 0;

	public MonkKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.BLAZE_ROD, 0, CC.gold + "Monk", description);
		getStartingItems().add(ABILITY_ITEM);
		GameHandler.getInstance().getSecondsHandler().add(() -> {
			cooldownSeconds = Math.max(0, cooldownSeconds - 1);
		});
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (event.isCancelled()) {
			return;
		}
		if (!(event.getDamager() instanceof Player)) {
			return;
		}
		if (!(event.getEntity() instanceof Player)) {
			return;
		}

		Player defender = (Player) event.getEntity();
		Player attacker = (Player) event.getDamager();

		ItemStack itemStack = attacker.getInventory().getItemInMainHand();
		if (!itemStack.isSimilar(ABILITY_ITEM)) {
			return;
		}
		if (!GameHandler.getInstance().getCurrentPhase().isEntityDamageEnabled()) {
			PlayerUtils.sendMessage(event.getDamager(), CC.red + "You can't use this yet!");
			event.setCancelled(true);
			return;
		}
		if (cooldownSeconds > 0) {
			KitsMain.sendCooldownMessage(attacker, cooldownSeconds);
			CooldownTitleAnimation.play(attacker, cooldownSeconds);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), attacker::updateInventory, 1L);
			event.setCancelled(true);
			return;
		}
		int rand = ThreadLocalRandom.current().nextInt(0, 35 + 1);
		ItemStack iRand = defender.getInventory().getItem(rand);
		defender.getInventory().setItem(rand, defender.getInventory().getItemInMainHand());
		defender.getInventory().setItemInMainHand(iRand);
		defender.updateInventory();
		defender.playSound(defender.getLocation(), Sound.ENTITY_TURTLE_EGG_HATCH, 10, 0);
		cooldownSeconds = COOLDOWN;
		event.setCancelled(true);
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}
}
