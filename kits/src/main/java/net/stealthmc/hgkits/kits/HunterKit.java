package net.stealthmc.hgkits.kits;

import com.google.common.collect.ImmutableMap;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.util.MapUtils;
import net.stealthmc.hgcore.api.AttachedListener;
import net.stealthmc.hgcore.game.entity.HGEntity;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.*;

/*

Coded by Fundryi & petomka

 */

public class HunterKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "I heard you like cooked steaks...",
			CC.gray + "well this kits fits you well.",
			CC.gray + "Killing animals will drop their cooked flesh!"
	);

	private static final Map<EntityType, Map<Material, Material>> SWAPPED_ITEMS = MapUtils.of(HashMap::new,
			EntityType.CHICKEN, ImmutableMap.of(Material.CHICKEN, Material.COOKED_CHICKEN),
			EntityType.COW, ImmutableMap.of(Material.BEEF, Material.COOKED_BEEF),
			EntityType.PIG, ImmutableMap.of(Material.PORKCHOP, Material.COOKED_PORKCHOP),
			EntityType.RABBIT, ImmutableMap.of(Material.RABBIT, Material.RABBIT_FOOT),
			EntityType.SHEEP, ImmutableMap.of(Material.MUTTON, Material.COOKED_MUTTON),
			EntityType.ZOMBIE, ImmutableMap.of(Material.ROTTEN_FLESH, Material.RABBIT_STEW)
	);

	private Set<UUID> attachedEntities = new HashSet<>();

	public HunterKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.COOKED_BEEF, 0, CC.gold + "Hunter", description);
	}

	private void onHunterKill(EntityDeathEvent event) {
		//the stored information is now useless, the entity died.
		attachedEntities.remove(event.getEntity().getUniqueId());
		//The killer could also be someone else! It is just guaranteed that the player at least damaged
		//the now dead entity, we don't know if he is the killer until we checked.
		Player killer = event.getEntity().getKiller();
		if (killer == null) {
			return;
		}
		if (!killer.getUniqueId().equals(this.getEntityId())) {
			return;
		}
		//Replace items corresponding to the now dead entity's type.
		Map<Material, Material> replaceMap = SWAPPED_ITEMS.get(event.getEntityType());
		if (replaceMap == null) {
			return;
		}
		event.getDrops().replaceAll(itemStack -> {
			Material newMaterial = replaceMap.get(itemStack.getType());
			if (newMaterial == null) {
				return itemStack;
			}
			int amount = itemStack.getAmount();
			return new ItemStack(newMaterial, amount);
		});
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		//Could also be called if the player using this kit was damaged!
		//-> Check if the player using this kit was actually the one damaging.
		if (!event.getDamager().getUniqueId().equals(this.getEntityId())) {
			return;
		}
		//Adding the HunterAdapter more than once to the damaged entity would
		//result in wasted computation time (it's probably insignificant, but
		//nevertheless)
		if (!attachedEntities.add(event.getEntity().getUniqueId())) {
			return;
		}
		//Add the HunterAdapter so we know when the entity that was damaged by
		//the player using the hunter kit died.
		HGEntity hgEntity = HGEntity.from(event.getEntity());
		hgEntity.getAttachedListeners().add(new HunterAdapter(event.getEntity()));
	}

	private class HunterAdapter extends AttachedListener {
		HunterAdapter(Entity entity) {
			super(entity.getUniqueId());
		}

		//This method will be called when the entity corresponding to this adapter died
		@Override
		public void onEntityDeath(EntityDeathEvent event) {
			//We notify the corresponding hunter that an entity died he at least damaged once
			onHunterKill(event);
		}
	}

}