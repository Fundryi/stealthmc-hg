package net.stealthmc.hgkits.kits;

import com.google.common.collect.ImmutableMap;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.LocationUtils;
import net.stealthmc.hgcore.api.AttachedListener;
import net.stealthmc.hgcore.game.entity.HGEntity;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/*

Coded by petomka | PAID

 */

public class SwitcherKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Hitting someone with a snowball",
			CC.gray + "will let you switch position!"
	);

	private static final double KNOCKBACK_STRENGTH = 0.6;

	private static final Map<Material, ItemStack> specialBlockBreaks = ImmutableMap.of(
			Material.SNOW, new ItemStack(Material.SNOWBALL, 2),
			Material.SNOW_BLOCK, new ItemStack(Material.SNOWBALL, 4)
	);

	public SwitcherKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.SNOWBALL, 0, CC.gold + "Switcher", description);
		getStartingItems().add(new ItemStack(Material.SNOWBALL, 16));
	}

	private static void switchPlaces(Entity switcher, Entity victim) {
		Location switcherLoc = switcher.getLocation();
		Location victimLoc = victim.getLocation();

		switcher.teleport(victimLoc);
		victim.teleport(switcherLoc);

		Vector dir = switcherLoc.toVector().subtract(victimLoc.toVector());
		dir.normalize().multiply(KNOCKBACK_STRENGTH);

		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), () ->
				victim.setVelocity(victim.getVelocity().add(dir)), 4L);
	}

	@Override
	public void onBlockBreak(BlockBreakEvent event) {
		specialBlockBreaks.computeIfPresent(event.getBlock().getType(), (material, itemStack) -> {
			event.setCancelled(true);
			Location target = LocationUtils.toCenterBlockLocation(event.getBlock());
			event.getBlock().getWorld().dropItemNaturally(target, itemStack);
			event.getPlayer().playSound(target, Sound.BLOCK_SNOW_BREAK, 1f, 1f);
			return itemStack;
		});
	}

	@Override
	public void onProjectileLaunch(ProjectileLaunchEvent event) {
		if (event.getEntity().getType() != EntityType.SNOWBALL) {
			return;
		}

		ProjectileSource source = event.getEntity().getShooter();
		if (!(source instanceof Player)) {
			return;
		}
		Player shooter = (Player) source;

		HGEntity entity = HGEntity.from(event.getEntity());
		entity.getAttachedListeners().add(
				new AttachedListener(event.getEntity().getUniqueId()) {

					@Override
					public void onEntityDamage(EntityDamageByEntityEvent event) {
						if (!event.getDamager().getUniqueId().equals(getEntityId())) {
							return;
						}
						if (!(event.getEntity() instanceof LivingEntity)) {
							return;
						}
						event.setCancelled(true);
						((LivingEntity) event.getEntity()).damage(0, shooter);
						switchPlaces(shooter, event.getEntity());
					}
				}
		);
	}
}
