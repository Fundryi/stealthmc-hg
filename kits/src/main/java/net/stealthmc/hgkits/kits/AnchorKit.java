package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Stream;

/*

Coded by petomka | PAID

 */

public class AnchorKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You're immune against any knockback",
			CC.gray + "dealt by a Player or Mob!",
			CC.gray + "You also have a 33% chance of giving no knockback!"
	);

	private static final double NO_KNOCKBACK_CHANCE = 0.33;

	private static final ItemStack HELMET_ITEM = Kit.createItemStack(Material.ANVIL, CC.red + "Anchor");

	private static final Sound HIT_SOUND = Sound.BLOCK_ANVIL_LAND;
	private static final float HIT_SOUND_PITCH_OUT = 0.75f;
	private static final float HIT_SOUND_PITCH_IN = 1.35f;

	public AnchorKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.ANVIL, 0, CC.gold + "Anchor", description);
	}

	@Override
	public void onBattleReady() {
		super.onBattleReady();
		Player player = Bukkit.getPlayer(this.getEntityId());
		if (player == null) {
			return;
		}
		player.getInventory().setHelmet(HELMET_ITEM);
	}

	@Override
	public void onDamage(EntityDamageEvent event) {
		if (event instanceof EntityDamageByEntityEvent) {
			return;
		}

		if (event.getCause() == EntityDamageEvent.DamageCause.CUSTOM) {
			return;
		}

		handleIncomingDamage(event);
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Projectile) {
			return;
		}

		if (event.getEntity().getUniqueId().equals(getEntityId())) {
			handleIncomingDamage(event);
		} else if (event.getDamager().getUniqueId().equals(getEntityId())) {
			handleDealDamage(event);
		}
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(HELMET_ITEM));
	}

	@Override
	public void onInventoryClick(InventoryClickEvent event) {
		PlayerInventory playerInventory = event.getWhoClicked().getInventory();
		if (!playerInventory.equals(event.getClickedInventory())) {
			return;
		}
		ItemStack itemStack = event.getCurrentItem();
		if (itemStack == null) {
			return;
		}
		if (!itemStack.isSimilar(HELMET_ITEM)) {
			return;
		}
		event.setCancelled(true);
	}

	private void handleDealDamage(EntityDamageByEntityEvent event) {
		if (!(event.getEntity() instanceof LivingEntity)) {
			return;
		}
		if (ThreadLocalRandom.current().nextDouble() > NO_KNOCKBACK_CHANCE) {
			return;
		}
		LivingEntity eventEntity = (LivingEntity) event.getEntity();

		Stream.of(event.getEntity(), event.getDamager())
				.filter(e -> e instanceof Player)
				.map(e -> (Player) e)
				.forEach(p -> p.playSound(event.getEntity().getLocation(), HIT_SOUND, 0.10f, HIT_SOUND_PITCH_OUT));


		handleAnchoredEntity(event, eventEntity);
	}

	private void handleIncomingDamage(EntityDamageEvent event) {
		if (!(event.getEntity() instanceof LivingEntity)) {
			return;
		}

		LivingEntity eventEntity = (LivingEntity) event.getEntity();

		Optional.of(event.getEntity())
				.filter(e -> e instanceof Player)
				.map(e -> (Player) e)
				.ifPresent(p -> p.playSound(event.getEntity().getLocation(), HIT_SOUND, 0.10f, HIT_SOUND_PITCH_IN));

		handleAnchoredEntity(event, eventEntity);
	}

	private void handleAnchoredEntity(EntityDamageEvent event, LivingEntity eventEntity) {
		if (eventEntity.getHealth() - event.getFinalDamage() <= 0) {
			return;
		}

		eventEntity.damage(event.getFinalDamage());
		event.setCancelled(true);
	}

}
