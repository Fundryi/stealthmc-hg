package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class RedstonerKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You spawn with",
			CC.gray + "32x Pistons",
			CC.gray + "128x Redstone",
			CC.gray + "16x Redstone Repeaters",
			CC.gray + "4x Dispensers",
			CC.gray + "16x Slime Balls",
			CC.gray + "2x Slime Blocks",
			CC.gray + "8x TNT"
	);

	private static final ItemStack ABILITY_ITEM_PISTON,
			ABILITY_ITEM_REDSTONE,
			ABILITY_ITEM_REDSTONE_REPEATERS,
			ABILITY_ITEM_DISPENSER,
			ABILITY_ITEM_SLIME_BALLS,
			ABILITY_ITEM_SLIME_BLOCK,
			ABILITY_ITEM_TNT;

	static {
		ABILITY_ITEM_PISTON = new ItemStack(Material.PISTON, 32);
		ABILITY_ITEM_REDSTONE = new ItemStack(Material.REDSTONE, 64);
		ABILITY_ITEM_REDSTONE_REPEATERS = new ItemStack(Material.REPEATER, 16);
		ABILITY_ITEM_DISPENSER = new ItemStack(Material.DISPENSER, 4);
		ABILITY_ITEM_SLIME_BALLS = new ItemStack(Material.SLIME_BALL, 16);
		ABILITY_ITEM_SLIME_BLOCK = new ItemStack(Material.SLIME_BLOCK, 2);
		ABILITY_ITEM_TNT = new ItemStack(Material.TNT, 8);
	}

	public RedstonerKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.PISTON, 0, CC.gold + "Redstoner", description);
		getStartingItems().add(ABILITY_ITEM_PISTON);
		getStartingItems().add(ABILITY_ITEM_REDSTONE);
		getStartingItems().add(ABILITY_ITEM_REDSTONE);
		getStartingItems().add(ABILITY_ITEM_REDSTONE_REPEATERS);
		getStartingItems().add(ABILITY_ITEM_DISPENSER);
		getStartingItems().add(ABILITY_ITEM_SLIME_BALLS);
		getStartingItems().add(ABILITY_ITEM_SLIME_BLOCK);
		getStartingItems().add(ABILITY_ITEM_TNT);
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(this::isStartingItem);
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (isStartingItem(event.getItemDrop().getItemStack())) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

}
