package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class CrafterKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Crafting made easy!",
			CC.gray + "Portable Crafting Table so no one can destroy it.",
			"",
			CC.red + CC.thickX + CC.gold + " This kit is WIP! " + CC.red + CC.thickX
	);

	private static final ItemStack CraftingItem = Kit.createItemStack(Material.NETHER_STAR, CC.red + "Portable Crafter");
	private static final ItemStack FurnaceItem = Kit.createItemStack(Material.NETHER_STAR, CC.red + "Portable Furnace");
	private static final GameHandler gameHandler = GameHandler.getInstance();

	public CrafterKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.CRAFTING_TABLE, 0, CC.gold + "Crafter", description);
		getStartingItems().add(CraftingItem);
		getStartingItems().add(FurnaceItem);
	}

	@Override
	public void onInteract(PlayerInteractEvent event) {
		onCrafting(event);
		onFurnace(event);
	}

	public void onCrafting(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		ItemStack itemStack = event.getPlayer().getInventory().getItemInMainHand();
		if (!itemStack.isSimilar(CraftingItem)) {
			return;
		}
		if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			player.openWorkbench(player.getLocation(), true);
		}
	}

	public void onFurnace(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		ItemStack itemStack = event.getPlayer().getInventory().getItemInMainHand();

		if (!itemStack.isSimilar(FurnaceItem)) {
			return;
		}
		if(!gameHandler.hasActiveInventory(player.getUniqueId())){
			Inventory FurnaceInv = Bukkit.createInventory(player, InventoryType.FURNACE);
			gameHandler.setActiveInventory(player.getUniqueId(), FurnaceInv);
		}
		if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			player.openInventory(gameHandler.getActiveInventory(player.getUniqueId()));
		}
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(CraftingItem));
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(FurnaceItem));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(CraftingItem) || event.getItemDrop().getItemStack().isSimilar(FurnaceItem)) {
			event.setCancelled(true);
		}
	}
}
