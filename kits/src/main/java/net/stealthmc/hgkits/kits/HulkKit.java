package net.stealthmc.hgkits.kits;

import com.google.common.collect.Sets;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcore.api.AttachedListener;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.tasks.CooldownTitleAnimation;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/*

Coded by petomka | PAID

 */

public class HulkKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Pick up animals, players or even TNT",
			CC.gray + "and toss them away!",
			"",
			CC.red + CC.thickX + CC.gold + " This kit is WIP! " + CC.red + CC.thickX
	);

	private static final int MAX_USES = 4;
	private static final int COOLDOWN = 10;
	private Set<UUID> undamageable = Sets.newHashSet();
	private int damageCooldownTick = 0;
	private int uses = 0;
	private int cooldownSeconds = 0;

	public HulkKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.SADDLE, 0, CC.gold + "Hulk", description);
		GameHandler.getInstance().getTicksHandler().add(() -> {
			damageCooldownTick = Math.max(0, damageCooldownTick - 1);
		});
		GameHandler.getInstance().getSecondsHandler().add(() -> {
			if (uses < MAX_USES) {
				return;
			}
			cooldownSeconds -= 1;
			if (cooldownSeconds == 0) {
				uses = 0;
			}
		});
	}

	private static void clearHulkAdapter(Player entity) {
		HGPlayer hgPlayer = HGPlayer.from(entity.getUniqueId());
		hgPlayer.getAttachedListeners().removeIf(playerAdapter ->
				playerAdapter instanceof HulkAdapter);
	}

	private static void addHulkAdapter(Entity entity) {
		if (!(entity instanceof Player)) {
			return;
		}
		HGPlayer hgPlayer = HGPlayer.from(entity.getUniqueId());
		hgPlayer.getAttachedListeners().add(new HulkAdapter(entity.getUniqueId()));
	}

	@Override
	public void onInteract(PlayerInteractEvent event) {
		if (event.getAction() != Action.LEFT_CLICK_AIR && event.getAction() != Action.LEFT_CLICK_BLOCK) {
			return;
		}
		ItemStack inHand = event.getItem();
		if (inHand != null && inHand.getType() != Material.AIR) {
			return;
		}
		throwPassenger(event.getPlayer(), event);
	}

	@Override
	public void onInteractEntity(PlayerInteractEntityEvent event) {
		if (!GameHandler.getInstance().getCurrentPhase().isEntityDamageEnabled()) {
			return;
		}
		if (!event.getPlayer().getUniqueId().equals(super.getEntityId())) {
			return;
		}
		ItemStack inHand = event.getPlayer().getInventory().getItemInMainHand();
		if (inHand != null && inHand.getType() != Material.AIR) {
			return;
		}

		if (event.getPlayer().getPassengers().isEmpty()) {
			return;
		}

		Entity rightClicked = event.getRightClicked();
		if (rightClicked == null) {
			return;
		}
		if (rightClicked instanceof Player) {
			if (!GameHandler.getInstance().isHandleablePlayer((Player) rightClicked)) {
				return;
			}
		}

		//COOLDOWN SYSTEM
		if (uses >= MAX_USES) {
			KitsMain.sendCooldownMessage(event.getPlayer(), cooldownSeconds);
			CooldownTitleAnimation.play(event.getPlayer(), cooldownSeconds);
			return;
		}
		uses += 1;
		if (uses >= MAX_USES) {
			cooldownSeconds = COOLDOWN;
		}

		//PICKUP PLAYER
		event.getPlayer().addPassenger(rightClicked);

		/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
		/*Comment out the line below to disable automatic ejection upon suffocation damage*/
		/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
		addHulkAdapter(rightClicked);

		undamageable.add(rightClicked.getUniqueId());
		event.setCancelled(true);
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (!event.getDamager().getUniqueId().equals(getEntityId())) {
			return;
		}
		Player player = (Player) event.getDamager();
		if (undamageable.contains(event.getEntity().getUniqueId())) {
			if (damageCooldownTick > 0) {
				event.setCancelled(true);
			} else {
				damageCooldownTick = 10;
			}
		}

		if (event.getDamager().getPassengers().isEmpty()) {
			return;
		}

		Entity passenger = event.getDamager().getPassengers().get(0);
		if (passenger != null) {
			if (player.getInventory().getItemInMainHand().getType() != Material.AIR) {
				return;
			}
			throwPassenger((Player) event.getDamager(), event);
		}
	}

	private void throwPassenger(Player player, Cancellable event) {
		if (player.getPassengers().isEmpty()) {
			return;
		}

		Entity toThrow = player.getPassengers().get(0);
		if (toThrow == null) {
			return;
		}

		player.eject();
		Vector direction = player.getLocation().getDirection().normalize().multiply(1.2);
		toThrow.setVelocity(direction);
		if (toThrow instanceof Player) {
			clearHulkAdapter((Player) toThrow);

			PlayerUtils.sendMessage(toThrow, CC.red + "You have been tossed away!");
		}
		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), () -> undamageable.removeIf(uuid -> uuid.equals(toThrow.getUniqueId())), 1L);
		event.setCancelled(true);
	}

	private static class HulkAdapter extends AttachedListener {

		HulkAdapter(UUID playerToAdapt) {
			super(playerToAdapt);
		}

		@Override
		public void onDamage(EntityDamageEvent event) {
			if (event.getCause() != EntityDamageEvent.DamageCause.SUFFOCATION) {
				return;
			}
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), () ->
					clearHulkAdapter((Player) event.getEntity()), 4L);
			Entity vehicle = event.getEntity().getVehicle();
			if (vehicle == null) {
				return;
			}
			vehicle.eject();
		}
	}

}
