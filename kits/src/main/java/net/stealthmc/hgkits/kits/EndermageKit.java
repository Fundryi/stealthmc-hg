package net.stealthmc.hgkits.kits;

import com.google.common.collect.Maps;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.specialblocks.EndermageSpecialBlock;
import net.stealthmc.hgkits.specialblocks.SpecialBlockHandler;
import net.stealthmc.hgkits.tasks.CooldownTitleAnimation;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/*

Coded by petomka | PAID

 */

public class EndermageKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Teleport all players in a",
			CC.gray + "3x3 area to a position of your desire.",
			CC.gray + "But keep in mind they have to be",
			CC.gray + "at least 5 Block above or below you!"
	);

	private static final ItemStack ABILITY_ITEM = Kit.createItemStack(Material.END_PORTAL_FRAME,
			CC.red + "Portal");

	private Map<EndermageSpecialBlock, Integer> placedBlocksSeconds = Maps.newHashMap();
	private int cooldownSeconds = 0;

	public EndermageKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.END_PORTAL_FRAME, 0, CC.gold + "Endermage", description);
		getStartingItems().add(ABILITY_ITEM);
		GameHandler.getInstance().getSecondsHandler().add(() -> {
			cooldownSeconds = Math.max(0, cooldownSeconds - 1);
			placedBlocksSeconds.replaceAll((endermageSpecialBlock, integer) -> integer - 1);
			placedBlocksSeconds.entrySet().stream()
					.filter(entry -> entry.getValue() <= 0)
					.map(Map.Entry::getKey)
					.forEach(EndermageSpecialBlock::remove);
			placedBlocksSeconds.values().removeIf(integer -> integer <= 0);
		});
	}

	@Override
	public void onInteract(PlayerInteractEvent event) {
		ItemStack inHand = event.getItem();
		if (inHand == null) {
			return;
		}
		if (!inHand.isSimilar(ABILITY_ITEM)) {
			return;
		}
		event.setCancelled(true); //dont use this particular endereye whatsoever
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
			return;
		}

		/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
		/*Comment the below condition to make the endermage able to use his kit in immortality phase*/
		/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
		//		if (!GameHandler.getInstance().getCurrentPhase().isDamageEnabled()) {
		//			PlayerUtils.sendMessage(event.getPlayer(), ChatColor.RED + "You can't use this yet!");
		//			return;
		//		}
		Block against = event.getClickedBlock();
		if (against == null) {
			return;
		}
		event.setCancelled(true);
		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		if (cooldownSeconds > 0) {
			KitsMain.sendCooldownMessage(event.getPlayer(), cooldownSeconds);
			CooldownTitleAnimation.play(event.getPlayer(), cooldownSeconds);
			return;
		}

		BlockState previousState = against.getState();
		EndermageSpecialBlock specialBlock = new EndermageSpecialBlock(against, event.getPlayer().getUniqueId(), previousState);
		if (EndermageSpecialBlock.isTargetBlocked(specialBlock.getTeleportTarget())) {
			PlayerUtils.sendMessage(event.getPlayer(), CC.red + "The target location is not suitable!");
			return;
		}

		against.setType(Material.END_PORTAL_FRAME);
		cooldownSeconds = 5;
		placedBlocksSeconds.put(specialBlock, 5);
		SpecialBlockHandler.getSpecialBlocks().add(specialBlock);
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

}
