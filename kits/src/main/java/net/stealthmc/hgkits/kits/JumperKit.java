package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class JumperKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You spawn with 8 Jumper Pearls",
			CC.gray + "You don't take any damage from teleporting.",
			"",
			CC.red + CC.thickX + CC.gold + " This kit is WIP! " + CC.red + CC.thickX
	);

	private static final ItemStack ABILITY_ITEM;
	private static final ItemStack ABILITY_ITEM_KILL;

	static {
		ABILITY_ITEM = new ItemStack(Material.ENDER_PEARL, 8);
		ItemMeta itemMeta = ABILITY_ITEM.getItemMeta();
		itemMeta.setDisplayName(CC.red + "Jumper Pearls");
		ABILITY_ITEM.setItemMeta(itemMeta);

		ABILITY_ITEM_KILL = new ItemStack(Material.ENDER_PEARL, 3);
		ABILITY_ITEM_KILL.setItemMeta(itemMeta);
	}

	public JumperKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.ENDER_PEARL, 0, CC.gold + "Jumper", description);
		getStartingItems().add(ABILITY_ITEM);
	}

	@Override
	public void onTeleport(PlayerTeleportEvent event) {
		Player player = event.getPlayer();
		TeleportCause cause = event.getCause();
		Location location = event.getTo();
		if (cause != TeleportCause.ENDER_PEARL) {
			return;
		}
		event.setCancelled(true);
		player.teleport(location);
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {

		if (!(event.getEntity() instanceof Player)) {
			return;
		}
		if (!(event.getDamager() instanceof Player)) {
			return;
		}

		Player player = (Player) event.getEntity();
		Player kitUser = (Player) event.getDamager();

		if (player.getUniqueId().equals(super.getEntityId())) { //FILTERS KITUSER OUT AS VICTIM
			return;
		}
		if (!(player.getHealth() - event.getFinalDamage() <= 0)) {
			return;
		}

		kitUser.playSound(kitUser.getLocation(), Sound.ENTITY_ENDERMAN_AMBIENT, 1, 1);
		kitUser.getInventory().addItem(ABILITY_ITEM_KILL);
		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), kitUser::updateInventory, 1L);
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}
}
