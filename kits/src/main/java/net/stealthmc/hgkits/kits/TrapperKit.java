package net.stealthmc.hgkits.kits;

import com.google.common.collect.ImmutableMap;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcommon.util.MapUtils;
import net.stealthmc.hgcore.api.AttachedListener;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgcore.game.entity.HGEntity;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.specialblocks.SpecialBlockHandler;
import net.stealthmc.hgkits.specialblocks.TrappingSpecialBlock;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.*;

/*

Coded by Fundryi

 */

public class TrapperKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "OOooof, the annoying kind of traps.",
			CC.gray + "You can place down trapwires, that will",
			CC.gray + "change into Spider webs if someone runs into them.",
			CC.gray + "Some mobs drop you stings, so keep an eye out."
	);

	private static final Map<EntityType, Map<Material, Material>> SWAPPED_ITEMS = MapUtils.of(HashMap::new,
			EntityType.SPIDER, ImmutableMap.of(Material.SPIDER_EYE, Material.STRING),
			EntityType.CAVE_SPIDER, ImmutableMap.of(Material.SPIDER_EYE, Material.STRING),
			EntityType.SHEEP, ImmutableMap.of(Material.WHITE_WOOL, Material.STRING),
			EntityType.COW, ImmutableMap.of(Material.LEATHER, Material.STRING),
			EntityType.ZOMBIE, ImmutableMap.of(Material.ROTTEN_FLESH, Material.STRING),
			EntityType.CHICKEN, ImmutableMap.of(Material.FEATHER, Material.STRING)
	);

	private static final ItemStack ABILITY_ITEM = new ItemStack(Material.STRING, 10);

	private Set<UUID> attachedEntities = new HashSet<>();

	public TrapperKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.STRING, 0, CC.gold + "Trapper", description);
		getStartingItems().add(new ItemStack(ABILITY_ITEM));
	}

	@Override
	public void onBlockPlace(BlockPlaceEvent event) {
		if (!GameHandler.getInstance().getCurrentPhase().isEntityDamageEnabled()) {
			PlayerUtils.sendMessage(event.getPlayer(), CC.red + "You can't use this yet!");
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
			return;
		}
		if (event.getBlock().getType() != Material.TRIPWIRE) {
			return;
		}
		SpecialBlockHandler.getSpecialBlocks().add(new TrappingSpecialBlock(event.getBlock(), event.getPlayer().getUniqueId()));
	}

	private void onTrapperKill(EntityDeathEvent event) {
		//the stored information is now useless, the entity died.
		attachedEntities.remove(event.getEntity().getUniqueId());
		//The killer could also be someone else! It is just guaranteed that the player at least damaged
		//the now dead entity, we don't know if he is the killer until we checked.
		Player killer = event.getEntity().getKiller();
		if (killer == null) {
			return;
		}
		if (!killer.getUniqueId().equals(this.getEntityId())) {
			return;
		}
		//Replace items corresponding to the now dead entity's type.
		Map<Material, Material> replaceMap = SWAPPED_ITEMS.get(event.getEntityType());
		if (replaceMap == null) {
			return;
		}
		event.getDrops().replaceAll(itemStack -> {
			Material newMaterial = replaceMap.get(itemStack.getType());
			if (newMaterial == null) {
				return itemStack;
			}
			int amount = itemStack.getAmount();
			return new ItemStack(newMaterial, amount);
		});
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		//Could also be called if the player using this kit was damaged!
		//-> Check if the player using this kit was actually the one damaging.
		if (!event.getDamager().getUniqueId().equals(this.getEntityId())) {
			return;
		}
		//Adding the HunterAdapter more than once to the damaged entity would
		//result in wasted computation time (it's probably insignificant, but
		//nevertheless)
		if (!attachedEntities.add(event.getEntity().getUniqueId())) {
			return;
		}
		//Add the HunterAdapter so we know when the entity that was damaged by
		//the player using the hunter kit died.
		HGEntity hgEntity = HGEntity.from(event.getEntity());
		hgEntity.getAttachedListeners().add(new TrapperKit.TrapperAdapter(event.getEntity()));
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

	private class TrapperAdapter extends AttachedListener {
		TrapperAdapter(Entity entity) {
			super(entity.getUniqueId());
		}

		//This method will be called when the entity corresponding to this adapter died
		@Override
		public void onEntityDeath(EntityDeathEvent event) {
			//We notify the corresponding hunter that an entity died he at least damaged once
			onTrapperKill(event);
		}
	}
}

