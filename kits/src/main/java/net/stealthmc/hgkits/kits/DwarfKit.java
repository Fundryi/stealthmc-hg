package net.stealthmc.hgkits.kits;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcommon.util.CommandUtils;
import net.stealthmc.hgcommon.util.ProgressBar;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.tasks.CooldownTitleAnimation;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

/*

Coded by petomka | PAID

 */

public class DwarfKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Sneaking will charge your burst of rage",
			CC.gray + "and everyone near you will be smashed away!",
			"",
			CC.red + CC.thickX + CC.gold + " This kit is WIP! " + CC.red + CC.thickX
	);
	private static final int CHARGE_TIME_MAX = 200;
	private static final int COOLDOWN = 15;

	private static ProgressBar progressBar = new ProgressBar(CHARGE_TIME_MAX);

	private int cooldownSeconds = 0;
	private int chargeTimeTicks = 0;

	public DwarfKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.GOLDEN_CHESTPLATE, 0, CC.gold + "Dwarf", description);
		if (playerToAdapt == null) {
			return;
		}

		HGPlayer hgPlayer = HGPlayer.from(playerToAdapt);
		Player player = hgPlayer.getPlayer();

		if (player == null) {
			KitsMain.getInstance().getLogger().log(Level.SEVERE, "Dwarf Kit Player is null");
		}
	}

	@Override
	public void onBattleReady() {
		super.onBattleReady();
		if (getEntityId() == null) {
			return;
		}
		HGPlayer hgPlayer = HGPlayer.from(getEntityId());
		if (hgPlayer == null) {
			return;
		}
		Player player = hgPlayer.getPlayer();
		GameHandler.getInstance().getTicksHandler().add(new ChargerRunnable(player));
		GameHandler.getInstance().getSecondsHandler().add(new ChatRunnable(player));
	}

	private boolean isDwarfing(Player player) {
		return player.isSneaking();
	}

	@Override
	public void onSneak(PlayerToggleSneakEvent event) {
		if (!GameHandler.getInstance().getCurrentPhase().isEntityDamageEnabled()) {
			PlayerUtils.sendMessage(event.getPlayer(), CC.red + "You can't use this yet!");
			event.setCancelled(true);
			return;
		}

		Player player = event.getPlayer();

		if (cooldownSeconds > 0 && event.isSneaking()) {
			KitsMain.sendCooldownMessage(player, cooldownSeconds);
			CooldownTitleAnimation.play(player, cooldownSeconds);
		}
	}

	private void knockBackExplosion(Player player, int timeCharged) {
		double power = (timeCharged / (double) CHARGE_TIME_MAX) * 4.0; //Do NOT go beyond 4.

		for (Entity other : player.getWorld().getEntities()) {
			if (other == player) {
				continue;
			}
			if (player.getLocation().distanceSquared(other.getLocation()) <= 16) {
				if (cooldownSeconds == 0) {
					cooldownSeconds = COOLDOWN;
				}

				Vector velocity = other.getLocation().subtract(player.getLocation())
						.toVector()
						.normalize()
						.multiply(power);

				velocity.setY(0.5);

				other.setVelocity(velocity);
			}
		}
	}

	@RequiredArgsConstructor
	private class ChargerRunnable implements Runnable {

		private final Player player;

		@Override
		public void run() {
			if (!KitsMain.getInstance().hasKit(player, DwarfKit.class)) {
				GameHandler.getInstance().getTicksHandler().removeIf(handler -> handler == this);
				return;
			}
			if (!isDwarfing(player) && chargeTimeTicks == 0) {
				return;
			}
			if (!isDwarfing(player)) {
				knockBackExplosion(player, chargeTimeTicks);
				chargeTimeTicks = 0;
				return;
			}
			if (cooldownSeconds > 0) {
				return;
			}
			chargeTimeTicks = Math.min(CHARGE_TIME_MAX, chargeTimeTicks + 1);
			String progress = progressBar.getBar(50, chargeTimeTicks);
			PlayerUtils.sendAction(player, progress, PlayerUtils.ActionPriority.HIGH);
		}

	}

	@AllArgsConstructor
	private class ChatRunnable implements Runnable {

		private final Player player;

		@Override
		public void run() {
			if (!KitsMain.getInstance().hasKit(player, DwarfKit.class)) {
				GameHandler.getInstance().getTicksHandler().removeIf(handler -> handler == this);
				return;
			}

			cooldownSeconds = Math.max(0, cooldownSeconds - 1);

			if (!isDwarfing(player)) {
				return;
			}

			if (cooldownSeconds > 0) {
				return;
			}

			PlayerUtils.sendMessage(player, CC.green + "Charged for " +
					CommandUtils.NO_DECIMAL_PLACES_FORMAT.format(
							(chargeTimeTicks / (double) CHARGE_TIME_MAX) * 100
					) + "%");
		}

	}

}
