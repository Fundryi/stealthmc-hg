package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.util.Vector;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class NeoKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			"",
			CC.red + CC.thickX + CC.gold + " This kit is WIP! " + CC.red + CC.thickX
	);


	public NeoKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.SPECTRAL_ARROW, 0, CC.gold + "Neo", description);
		GameHandler.getInstance().getTicksHandler().add(() -> {
			if (!GameHandler.getInstance().getCurrentPhase().isEntityDamageEnabled()) {
				return;
			}
			if (playerToAdapt == null) {
				return;
			}
			Player player = Bukkit.getPlayer(playerToAdapt);
			if (player == null) {
				return;
			}
			if (!KitsMain.getInstance().hasKit(player, this.getClass())) {
				return;
			}
			for (Entity entity : player.getNearbyEntities(3, 5, 3)) {
				if (!(entity instanceof Projectile)) {
					continue;
				}
				Projectile projectile = (Projectile) entity;
				if (projectile.getTicksLived() <= 2) {
					continue;
				}
				Entity lookingAt = getLookingAt(player);
				if (lookingAt == player) {
					projectile.setVelocity(projectile.getVelocity().multiply(-1.1));
				}
			}
		});
	}

	private @Nullable
	Player getLookingAt(@Nonnull Player player) {
		Location location = player.getLocation();
		Vector dir = location.getDirection().normalize();
		World world = player.getWorld();
		for (int i = 0; i < 25; i++) {
			Player lookingAt = world.getNearbyEntities(location.add(dir), 0.5, 1.25, 0.5)
					.stream()
					.filter(e -> e instanceof Player)
					.map(e -> (Player) e)
					.filter(p -> !p.getUniqueId().equals(player.getUniqueId()))
					.filter(GameHandler.getInstance()::isHandleablePlayer)
					.filter(GameHandler.getInstance()::isAlive)
					.findFirst()
					.orElse(null);
			if (lookingAt != null) {
				return lookingAt;
			}
		}
		return null;
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (!(event.getDamager() instanceof Projectile) || !(event.getEntity() instanceof Player)) {
			return;
		}
		event.setCancelled(true);

		Player player = (Player) event.getEntity();
		Projectile projectile = (Projectile) event.getDamager();
		projectile.setShooter(player);
		projectile.setVelocity(projectile.getVelocity().multiply(-1.1));
	}

}
