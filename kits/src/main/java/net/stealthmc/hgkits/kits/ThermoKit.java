package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.tasks.CooldownTitleAnimation;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class ThermoKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Lets you change Water to Lava,",
			CC.gray + "also works the other way around!",
			CC.gray + "There is a 200 blocks limit",
			CC.gray + "and a 20-second cooldown."
	);

	private static final int MAX_CHANGES = 200;
	private static final int COOLDOWN = 20;
	private static ItemStack ABILITY_ITEM;

	static {
		ABILITY_ITEM = Kit.createItemStack(Material.DAYLIGHT_DETECTOR, CC.red + "Swapper");
	}

	private BlockFace[] sides = {BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST, BlockFace.UP, BlockFace.DOWN};
	private int cooldownSeconds = 0;

	public ThermoKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.DAYLIGHT_DETECTOR, 0, CC.gold + "Thermo", description);
		getStartingItems().add(ABILITY_ITEM);
		GameHandler.getInstance().getSecondsHandler().add(() -> {
			cooldownSeconds = Math.max(0, cooldownSeconds - 1);
		});
	}

	@Override
	public void onBlockPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		ItemStack inHand = event.getPlayer().getInventory().getItemInMainHand();
		if (!inHand.isSimilar(ABILITY_ITEM)) {
			return;
		}
		if (!GameHandler.getInstance().getCurrentPhase().isEntityDamageEnabled()) {
			PlayerUtils.sendMessage(event.getPlayer(), CC.red + "You can't use this yet!");
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
			return;
		}
		event.setCancelled(true);
		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		if (cooldownSeconds > 0) {
			event.setCancelled(true);
			KitsMain.sendCooldownMessage(event.getPlayer(), cooldownSeconds);
			CooldownTitleAnimation.play(event.getPlayer(), cooldownSeconds);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
			return;
		}

		Material replaced = event.getBlockReplacedState().getType();
		boolean lava = false;

		if (replaced == Material.LAVA) {
			lava = true;
		} else if (replaced != Material.WATER) {
			PlayerUtils.sendMessage(player, CC.red + "Ohh, Looks like the Swapper didn't find any water or lava.");
			return;
		}

		final HashSet<Block> changedBlocks = getChangedBlocks(event.getBlockReplacedState().getBlock(), lava);
		if (changedBlocks.size() > MAX_CHANGES) {
			event.setCancelled(true);
			PlayerUtils.sendMessage(player, CC.red + String.format("This is too much! It can only change up to %s blocks at a time!", MAX_CHANGES));
			return;
		}

		cooldownSeconds = COOLDOWN;
		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);

		final boolean isLava = lava;

		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), () -> {
			for (Block block : changedBlocks) {
				if (isLava)
					block.setType(Material.WATER, false);
				else
					block.setType(Material.LAVA, false);
			}
			if (isLava) {
				event.getBlockReplacedState().getBlock().setType(Material.WATER, false);
				for (Block block : changedBlocks) {
					if (block.getType() == Material.COBBLESTONE)
						block.setType(Material.WATER, false);
					else if (block.getType() == Material.OBSIDIAN)
						block.setType(Material.LAVA, false);
				}
			} else {
				event.getBlockReplacedState().getBlock().setType(Material.LAVA, false);
				for (Block block : changedBlocks) {
					if (block.getType() == Material.COBBLESTONE)
						block.setType(Material.WATER, false);
					else if (block.getType() == Material.OBSIDIAN)
						block.setType(Material.LAVA, false);
				}
			}
		}, 3L);
	}

	private HashSet<Block> getChangedBlocks(Block block, boolean lava) {
		HashSet<Block> blocks = new HashSet<Block>();
		therminate(blocks, block, lava);
		return blocks;
	}

	private void therminate(HashSet<Block> blocks, Block block, boolean lava) {
		if (blocks.size() > MAX_CHANGES + 1)
			return;
		for (BlockFace side : sides) {
			Block sideBlock = block.getRelative(side);
			if (blocks.contains(sideBlock)) {
				continue;
			}
			Material setTo = null;
			if (lava) {
				if (sideBlock.getType() == Material.LAVA) {
					setTo = Material.WATER;
				}
				if (sideBlock.getType() == Material.OBSIDIAN) {
					setTo = Material.WATER;
				}
				if (sideBlock.getType() == Material.COBBLESTONE) {
					setTo = Material.WATER;
				}
			} else {
				if (sideBlock.getType() == Material.WATER) {
					setTo = Material.LAVA;
				}
				if (sideBlock.getType() == Material.OBSIDIAN) {
					setTo = Material.LAVA;
				}
				if (sideBlock.getType() == Material.COBBLESTONE) {
					setTo = Material.LAVA;
				}
			}
			if (setTo != null) {
				blocks.add(sideBlock);
				therminate(blocks, sideBlock, lava);
			}
		}
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

}
