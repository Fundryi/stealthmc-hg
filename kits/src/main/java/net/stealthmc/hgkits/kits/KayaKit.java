package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.LocationUtils;
import net.stealthmc.hgcore.HGBukkitMain;
import net.stealthmc.hgcore.game.CraftingRecipeHandler;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.specialblocks.DisappearingSpecialBlock;
import net.stealthmc.hgkits.specialblocks.SpecialBlockHandler;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapelessRecipe;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/*

Coded by petomka | PAID

 */

public class KayaKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Place grass blocks that disappear",
			CC.gray + "as soon as someone else than you tries",
			CC.gray + "to step on them."
	);
	private static final int chance = 75;
	private static ShapelessRecipe grassRecipe;

	static {
		grassRecipe = new ShapelessRecipe(new NamespacedKey(HGBukkitMain.getInstance(), "KayaGrass"), new ItemStack(Material.GRASS_BLOCK));
		grassRecipe.addIngredient(1, Material.WHEAT_SEEDS);
		grassRecipe.addIngredient(1, Material.DIRT);

		CraftingRecipeHandler.registerRecipe(grassRecipe);
		KitsMain.getInstance().getSpecialRecipes().add(grassRecipe);
	}

	public KayaKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.GRASS_BLOCK, 0, CC.gold + "Kaya", description);
		getStartingItems().add(new ItemStack(Material.GRASS_BLOCK, 25));
	}

	@Override
	public boolean canCraftSpecialRecipe(Recipe recipe) {
		return recipe.equals(grassRecipe);
	}

	@Override
	public void onBlockBreak(final BlockBreakEvent event) {
		if (!(event.getBlock().getType() == Material.GRASS
				|| event.getBlock().getType() == Material.TALL_GRASS
				|| event.getBlock().getType() == Material.FERN
				|| event.getBlock().getType() == Material.LARGE_FERN)) {
			return;
		}
		event.getBlock().setType(Material.AIR);
		event.setCancelled(true);
		if (ThreadLocalRandom.current().nextInt(100) <= chance) {
			event.getBlock().getWorld().dropItemNaturally(LocationUtils.toCenterLocation(event.getBlock()),
					new ItemStack(Material.WHEAT_SEEDS));
		}
	}

	@Override
	public void onBlockPlace(BlockPlaceEvent event) {
		if (event.getBlock().getType() != Material.GRASS_BLOCK) {
			return;
		}
		SpecialBlockHandler.getSpecialBlocks().add(new DisappearingSpecialBlock(event.getBlock(),
				event.getPlayer().getUniqueId()));
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.getType() == Material.GRASS_BLOCK);
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().getType() == Material.GRASS_BLOCK) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}
}
