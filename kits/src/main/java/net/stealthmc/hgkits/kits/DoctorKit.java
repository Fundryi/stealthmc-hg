package net.stealthmc.hgkits.kits;

import com.google.common.collect.ImmutableList;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcommon.util.StringUtils;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi & petomka

 */

public class DoctorKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You can heal players from their sickness",
			CC.gray + "with your scalpel.",
			CC.gray + "It will remove EVERY potion effect!",
			CC.gray + "Ohh and you're immune to any damage Effects!"
	);

	private static final ItemStack ABILITY_ITEM = Kit.createItemStack(Material.SHEARS,
			CC.red + "Scalpel");

	private List<EntityDamageEvent.DamageCause> filteredCauses = ImmutableList.of(
			EntityDamageEvent.DamageCause.POISON,
			EntityDamageEvent.DamageCause.WITHER,
			EntityDamageEvent.DamageCause.STARVATION,
			EntityDamageEvent.DamageCause.MAGIC
	);

	public DoctorKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.SHEARS, 0, CC.gold + "Doctor", description);
		getStartingItems().add(ABILITY_ITEM);
	}

	@Override
	public void onInteractEntity(PlayerInteractEntityEvent event) {
		ItemStack itemStack = event.getPlayer().getInventory().getItemInMainHand();
		if (!itemStack.isSimilar(ABILITY_ITEM)) {
			return;
		}
		event.setCancelled(true);

		if (!(event.getRightClicked() instanceof LivingEntity)) {
			return;
		}
		if (!GameHandler.getInstance().getCurrentPhase().isEntityDamageEnabled()) {
			return;
		}

		//cleanseEntity(event.getPlayer());
		cleanseEntity((LivingEntity) event.getRightClicked());

		if (event.getRightClicked() instanceof Player) {
			PlayerUtils.sendMessage(event.getRightClicked(), CC.green + "You have been healed by a doctor!");
			PlayerUtils.sendMessage(event.getPlayer(), CC.green + "You have healed " + CC.red + event.getRightClicked().getName() + CC.green + "!");
		} else {
			PlayerUtils.sendMessage(event.getPlayer(), CC.green + "You have healed a " + CC.red + StringUtils.normalizeEnumName(event.getRightClicked().getType()) + CC.green + "!");
		}
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		ItemStack itemStack = ((Player) event.getDamager()).getInventory().getItemInMainHand();
		if (!itemStack.isSimilar(ABILITY_ITEM)) {
			return;
		}

		if (!(event.getEntity() instanceof LivingEntity)) {
			return;
		}
		if (!GameHandler.getInstance().getCurrentPhase().isEntityDamageEnabled()) {
			return;
		}
		//cleanseEntity((Player) event.getDamager());
		cleanseEntity((LivingEntity) event.getEntity());

		if (event.getDamager() instanceof Player) {
			PlayerUtils.sendMessage(event.getEntity(), CC.green + "You have been healed by a doctor!");
			PlayerUtils.sendMessage(event.getDamager(), CC.green + "You have healed " + CC.red + event.getEntity().getName() + CC.green + "!");
		} else {
			PlayerUtils.sendMessage(event.getDamager(), CC.green + "You have healed a " + CC.red + StringUtils.normalizeEnumName(event.getEntity().getType()) + CC.green + "!");
		}
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

	@Override
	public void onDamage(EntityDamageEvent event) {
		if (!event.getEntity().getUniqueId().equals(getEntityId())) {
			return;
		}
		if (filteredCauses.contains(event.getCause())) {
			event.setCancelled(true);
		}
	}

	@Override
	public void onItemDamage(PlayerItemDamageEvent event) {
		if (event.getItem().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

	private void cleanseEntity(LivingEntity entity) {
		entity.getActivePotionEffects().stream()
				.map(PotionEffect::getType)
				.forEach(entity::removePotionEffect);
	}

}
