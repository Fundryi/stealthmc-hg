package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class PoseidonKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You are stronger and faster in the water!",
			CC.gray + "So don't forget to lure players into water."
	);
	private static final int damageMultiplier = 0;
	private static final int speedMultiplier = 4;

	public PoseidonKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.LILY_PAD, 0, CC.gold + "Poseidon", description);
	}

	@Override
	public void onMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if (player.getLocation().getBlock().isLiquid()) {
			if (event.getPlayer().getRemainingAir() < event.getPlayer().getMaximumAir()) {
				player.setRemainingAir(event.getPlayer().getMaximumAir());
			}
			player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 29, damageMultiplier));
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 49, speedMultiplier));
		}
	}
}
