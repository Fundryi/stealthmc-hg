package net.stealthmc.hgkits.kits;

import com.google.common.collect.ImmutableList;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class VikingKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You're very strong with axes,",
			CC.gray + "in fact you deal 0.5 heart more damage",
			CC.gray + "with every axe in the game.",
			"",
			CC.red + CC.thickX + CC.gold + " This kit is WIP! " + CC.red + CC.thickX
	);

	private static final double VIKING_DAMAGE = 1;

	private static final List<Material> AXES = ImmutableList.of(
			Material.WOODEN_AXE,
			Material.STONE_AXE,
			Material.IRON_AXE,
			Material.GOLDEN_AXE,
			Material.DIAMOND_AXE
	);

	public VikingKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.IRON_AXE, 0, CC.gold + "Viking", description);
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (!(event.getDamager() instanceof Player)) {
			return;
		}
		ItemStack inHand = ((Player) event.getDamager()).getInventory().getItemInMainHand();
		if (!AXES.contains(inHand.getType())) {
			return;
		}
		event.setDamage(event.getDamage() + VIKING_DAMAGE);
	}

}
