package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.specialblocks.LandmineSpecialBlock;
import net.stealthmc.hgkits.specialblocks.SpecialBlockHandler;
import org.bukkit.Material;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/*

Coded by petomka | PAID

 */

public class DemomanKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Place stone plates on top of",
			CC.gray + "gravel to create landmines."
	);

	public static Material defaultTrigger = Material.STONE_PRESSURE_PLATE;

	public static Material[] bonusTrigger = new Material[]{
			Material.ACACIA_PRESSURE_PLATE,
			Material.BIRCH_PRESSURE_PLATE,
			Material.DARK_OAK_PRESSURE_PLATE,
			Material.JUNGLE_PRESSURE_PLATE,
			Material.OAK_PRESSURE_PLATE,
			Material.SPRUCE_PRESSURE_PLATE
	};

	public static float explosionYield = 5f;

	public DemomanKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.STONE_PRESSURE_PLATE, 0, CC.gold + "Demoman", description);
		getStartingItems().add(new ItemStack(Material.GRAVEL, 6));
		getStartingItems().add(new ItemStack(Material.STONE_PRESSURE_PLATE, 6));
	}

	@Override
	public void onBlockPlace(BlockPlaceEvent event) {
		onDefaultDemomanTrigger(event);
		onBonusDemomanTrigger(event);
	}

	//Default Demoman Trigger
	private void onDefaultDemomanTrigger(BlockPlaceEvent event) {
		if (event.getBlock().getType() != defaultTrigger) {
			return;
		}
		if (!(event.getBlock().getLocation().subtract(0, 0.5, 0).getBlock().getType() == Material.GRAVEL)) {
			return;
		}
		SpecialBlockHandler.getSpecialBlocks().add(new LandmineSpecialBlock(event.getBlock(), event.getPlayer().getUniqueId()));
	}

	//Bonus Trigger (Will change from time to time.)
	private void onBonusDemomanTrigger(BlockPlaceEvent event) {
		if (Arrays.stream(bonusTrigger).noneMatch(material -> event.getBlock().getType().equals(material))) {
			return;
		}

		if (!(event.getBlock().getLocation().subtract(0, 0.5, 0).getBlock().getType().name().endsWith("WOOD"))) {
			return;
		}
		SpecialBlockHandler.getSpecialBlocks().add(new LandmineSpecialBlock(event.getBlock(), event.getPlayer().getUniqueId()));
	}

}
