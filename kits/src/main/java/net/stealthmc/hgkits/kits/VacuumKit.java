package net.stealthmc.hgkits.kits;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcommon.util.MathUtils;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/*

Coded by petomka | PAID

 */

public class VacuumKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Well you like to clean stuff.",
			CC.gray + "If you right click your Black Hole",
			CC.gray + "it will pull every Player,",
			CC.gray + "Entity or Items towards you."
	);

	private static final List<EntityType> FILTER_ENTITY_TYPES = ImmutableList.of(
			EntityType.PAINTING,
			EntityType.ITEM_FRAME,
			EntityType.ARMOR_STAND,
			//EntityType.COMPLEX_PART,
			EntityType.LEASH_HITCH,
			EntityType.FALLING_BLOCK,
			EntityType.FISHING_HOOK,
			EntityType.LIGHTNING,
			EntityType.FIREWORK,
			EntityType.UNKNOWN
	);

	private static final ItemStack ABILITY_ITEM = Kit.createItemStack(Material.ENDER_EYE, CC.red + "Black Hole");

	private static final double SOUND_RADIUS = 15.0;

	private static final Sound WARNING_SOUND = Sound.ENTITY_ENDER_DRAGON_FLAP;

	private static final double VACUUM_RADIUS = 7.0;

	private static final double PULL_STRENGTH = 1.75;

	private static final double NEARBY_DAMAGE_RADIUS = 2;

	private static final double NEARBY_DAMAGE = 4;

	private int cooldownTicks = 0;

	private Set<UUID> recentlyDamaged = Sets.newHashSet();

	public VacuumKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.ENDER_EYE, 0, CC.gold + "Vacuum", description);
		GameHandler.getInstance().getTicksHandler().add(() -> {
			cooldownTicks = Math.max(0, cooldownTicks - 1);
		});
		GameHandler.getInstance().getSecondsHandler().add(() -> {
			recentlyDamaged.clear();
		});
		getStartingItems().add(ABILITY_ITEM);
	}

	private static void playSound(Player origin, Entity entity) {
		if (entity instanceof Player) {
			((Player) entity).playSound(origin.getLocation(), WARNING_SOUND, 20f, 1.5f);
		}
	}

	private static Vector getCappedVelocity(Vector original, Vector add) {
		Vector result = new Vector(
				MathUtils.clamp(-4, 4, original.getX() + add.getX()),
				MathUtils.clamp(-4, 4, original.getY() + add.getY()),
				MathUtils.clamp(-4, 4, original.getZ() + add.getZ())
		);
		if (result.length() > 4) {
			return result.normalize().multiply(4);
		}
		return result;
	}

	private void applyDirection(Player origin, Entity entity) {
		if (entity instanceof Player) {
			HGPlayer hgPlayer = HGPlayer.from(entity.getUniqueId());
			if (hgPlayer.isSpectating()) {
				return;
			}
			if (entity.getUniqueId().equals(super.getEntityId())) {
				return;
			}
		}

		Vector dir = origin.getLocation().toVector().subtract(entity.getLocation().toVector());
		dir.normalize().multiply(PULL_STRENGTH);
		entity.setVelocity(getCappedVelocity(entity.getVelocity(), dir));
	}

	@Override
	public void onInteract(PlayerInteractEvent event) {
		if (event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) {
			return;
		}
		ItemStack inHand = event.getItem();
		if (inHand == null) {
			return;
		}
		if (!inHand.isSimilar(ABILITY_ITEM)) {
			return;
		}
		if (!GameHandler.getInstance().getCurrentPhase().isEntityDamageEnabled()) {
			PlayerUtils.sendMessage(event.getPlayer(), CC.red + "You can't use this yet!");
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
			return;
		}
		event.setCancelled(true);
		if (cooldownTicks > 0) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
			return;
		}
		event.setCancelled(true);
		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);

		vacuum(event.getPlayer(), event.getPlayer().getWorld()
				.getNearbyEntities(event.getPlayer().getLocation(), VACUUM_RADIUS, VACUUM_RADIUS, VACUUM_RADIUS));

		cooldownTicks = 12;
	}

	private void vacuum(Player origin, Collection<Entity> toCheck) {
		double rSq = VACUUM_RADIUS * VACUUM_RADIUS;
		double dmgSq = NEARBY_DAMAGE_RADIUS * NEARBY_DAMAGE_RADIUS;
		double soundSq = SOUND_RADIUS * SOUND_RADIUS;

		//Filter out unwanted entitis and ones in other worlds
		toCheck = toCheck.stream()
				.filter(entity -> !FILTER_ENTITY_TYPES.contains(entity.getType()))
				.filter(entity -> entity.getWorld().equals(origin.getWorld()))
				.collect(Collectors.toList());

		//Play the warning sound
		toCheck.stream()
				.filter(entity -> entity.getLocation().distanceSquared(origin.getLocation()) <= soundSq)
				.forEach(entity -> playSound(origin, entity));

		//Accelerate the entity towards the player
		toCheck.stream()
				.filter(entity -> entity.getLocation().distanceSquared(origin.getLocation()) <= rSq)
				.forEach(entity -> applyDirection(origin, entity));

		//Apply damage if not recently damaged
		toCheck.stream()
				.filter(entity -> entity instanceof LivingEntity)
				.map(entity -> (LivingEntity) entity)
				.filter(entity -> entity.getLocation().distanceSquared(origin.getLocation()) <= dmgSq)
				.forEach(entity -> applyDamage(origin, entity));

	}

	private void applyDamage(Player player, LivingEntity entity) {
		if (!recentlyDamaged.add(entity.getUniqueId())) {
			return;
		}
		if (player.getUniqueId().equals(entity.getUniqueId())) {
			return;
		}
		if (entity instanceof Player) {
			HGPlayer hgEntity = HGPlayer.from(entity.getUniqueId());
			hgEntity.setLastDamager(player.getUniqueId());
		}
		entity.damage(NEARBY_DAMAGE, player);
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}
}
