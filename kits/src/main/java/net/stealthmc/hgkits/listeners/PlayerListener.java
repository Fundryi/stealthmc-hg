package net.stealthmc.hgkits.listeners;

import net.stealthmc.hgcore.api.AttachedListener;
import net.stealthmc.hgcore.game.CraftingRecipeHandler;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.specialblocks.SpecialBlockHandler;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.UUID;

public class PlayerListener implements Listener {

	private static ItemStack KIT_SELECTOR;

	static {
		KIT_SELECTOR = new ItemStack(Material.CHEST);
		ItemMeta meta = KIT_SELECTOR.getItemMeta();
		meta.setDisplayName(ChatColor.GOLD + "Kit Selector");
		KIT_SELECTOR.setItemMeta(meta);
	}

	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onItemCraft(CraftItemEvent event) {
		Recipe recipe = event.getRecipe();
		if (recipe instanceof ShapedRecipe) {
			handleShapedRecipeCraft(event, (ShapedRecipe) recipe);
			return;
		}
		if (recipe instanceof ShapelessRecipe) {
			handleShapelessRecipeCraft(event, (ShapelessRecipe) recipe);
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onJoin(PlayerJoinEvent event) {
		KitsMain.getInstance().updatePlayerKitScoreboard(event.getPlayer());
		event.getPlayer().getInventory().addItem(KIT_SELECTOR);

		class KitSelectorAdapter extends AttachedListener {

			private KitSelectorAdapter(UUID playerToAdapt) {
				super(playerToAdapt);
			}

			@Override
			public void onInteract(PlayerInteractEvent event) {
				ItemStack itemStack = event.getPlayer().getInventory().getItemInMainHand();

				if (!itemStack.isSimilar(KIT_SELECTOR)) {
					return;
				}

				event.getPlayer().chat("/kits");
			}

			@Override
			public void onBattleReady() {
				HGPlayer hgPlayer = HGPlayer.from(getEntityId());
				hgPlayer.getAttachedListeners().removeIf(playerAdapter -> playerAdapter == this);
			}

		}

		HGPlayer hgPlayer = HGPlayer.from(event.getPlayer());
		hgPlayer.getAttachedListeners().add(new KitSelectorAdapter(event.getPlayer().getUniqueId()));

	}

	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onInteract(PlayerInteractEvent event) {
		Block block = event.getClickedBlock();
		if (block == null) {
			return;
		}
		SpecialBlockHandler.getSpecialBlockFrom(block).ifPresent(specialBlock -> {
			if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				specialBlock.onRightClick(event.getPlayer());
				return;
			}
			if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
				specialBlock.onLeftClick(event.getPlayer());
			}
		});
	}

	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onBlockBreak(BlockBreakEvent event) {
		SpecialBlockHandler.getSpecialBlockFrom(event.getBlock())
				.ifPresent(specialBlock -> specialBlock.onBlockBreak(event));
	}

	private void handleShapedRecipeCraft(CraftItemEvent event, ShapedRecipe recipe) {
		Recipe specialRecipe = KitsMain.getInstance().getSpecialRecipes()
				.stream()
				.filter(r -> r instanceof ShapedRecipe)
				.map(r -> (ShapedRecipe) r)
				.filter(sr -> CraftingRecipeHandler.areEqual(sr, recipe))
				.findFirst()
				.orElse(null);
		handleSpecialRecipeCraft(event, specialRecipe);
	}

	private void handleShapelessRecipeCraft(CraftItemEvent event, ShapelessRecipe recipe) {
		Recipe specialRecipe = KitsMain.getInstance().getSpecialRecipes()
				.stream()
				.filter(r -> r instanceof ShapelessRecipe)
				.map(r -> (ShapelessRecipe) r)
				.filter(slr -> CraftingRecipeHandler.areEqual(slr, recipe))
				.findFirst()
				.orElse(null);
		handleSpecialRecipeCraft(event, specialRecipe);
	}

	private void handleSpecialRecipeCraft(CraftItemEvent event, Recipe specialRecipe) {
		if (specialRecipe == null) {
			return;
		}
		HGPlayer hgPlayer = HGPlayer.from(event.getWhoClicked().getUniqueId());
		boolean allow = hgPlayer.getAttachedListeners()
				.stream()
				.filter(playerAdapter -> playerAdapter instanceof Kit)
				.map(playerAdapter -> (Kit) playerAdapter)
				.anyMatch(kit -> kit.canCraftSpecialRecipe(specialRecipe));
		if (allow) {
			return;
		}
		event.setCancelled(true);
	}

}
