package net.stealthmc.hgkits.listeners;

import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.kits.DemomanKit;
import net.stealthmc.hgkits.kits.TankKit;
import net.stealthmc.hgkits.kits.TurtleKit;
import net.stealthmc.hgkits.model.SpecialBlock;
import net.stealthmc.hgkits.specialblocks.LandmineSpecialBlock;
import net.stealthmc.hgkits.specialblocks.LauncherSpecialBlock;
import net.stealthmc.hgkits.specialblocks.SpecialBlockHandler;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockListener implements Listener {

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onExplode(BlockExplodeEvent event) {
		SpecialBlock specialBlock = SpecialBlockHandler.getSpecialBlockFrom(event.getBlock()).orElse(null);
		if (specialBlock == null) {
			return;
		}
		if (specialBlock instanceof LandmineSpecialBlock) {
			handleLandmineExplosion((LandmineSpecialBlock) specialBlock);
		}
	}

	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onBlockPlace(BlockPlaceEvent event) {
		if (event.getBlock().getType() == LauncherSpecialBlock.LAUNCHER_ITEM.getType()) {
			SpecialBlockHandler.getSpecialBlocks().add(new LauncherSpecialBlock(event.getBlock(),
					event.getPlayer().getUniqueId()));
		}
	}

	private void handleLandmineExplosion(LandmineSpecialBlock specialBlock) {
		//float yield = specialBlock.getYield();
		float yield = DemomanKit.explosionYield;
		float radiusSq = yield * yield;

		Location blockLocation = specialBlock.getBlock().getLocation();
		GameHandler.getInstance().getPlayingPlayersStream()
				.filter(player -> {
					Location location = player.getLocation();
					if (!location.getWorld().equals(blockLocation.getWorld())) {
						return false;
					}
					return location.distanceSquared(blockLocation) <= radiusSq;
				})
				.forEach(player -> {
					HGPlayer hgPlayer = HGPlayer.from(player);
					hgPlayer.setLastDamager(specialBlock.getBlockOwner());
					double newHealth = Math.max(0, player.getHealth() - yield);
					if ((KitsMain.getInstance().hasKit(player, TankKit.class)) || (KitsMain.getInstance().hasKit(player, TurtleKit.class))) {
						return;
					}
					player.setHealth(newHealth);
				});
	}

}
