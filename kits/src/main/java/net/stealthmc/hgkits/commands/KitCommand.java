package net.stealthmc.hgkits.commands;

import com.google.common.collect.ImmutableList;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcommon.bukkit.inventory.InventoryMenu;
import net.stealthmc.hgcommon.bukkit.inventory.PagedInvMenu;
import net.stealthmc.hgcommon.util.CommandUtils;
import net.stealthmc.hgcommon.util.TerConsumer;
import net.stealthmc.hgcore.HGBukkitMain;
import net.stealthmc.hgcore.config.Messages;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class KitCommand implements TabExecutor {

	/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	/*Used for debugging, allows changing kit in a running game*/
	/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	private static final boolean ALLOW_MID_GAME_CHANGE = true;

	private static final String INVENTORY_TITLE = CC.blue + "Choose your kit:";

	private static final String CANNOT_SELECT_KIT = CC.red + "You don't have access to " + CC.gold + "%s" + CC.red + " kit!";

	static List<String> completeKits(String token) {
		return CommandUtils.copyPartialMatches(token, KitsMain.getInstance().getRegisteredKitNames()
				.values()
				.stream()
				.flatMap(Collection::stream)
				.collect(Collectors.toList()));
	}

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String alias, String[] args) {
		if (!(commandSender instanceof Player)) {
			Messages messages = HGBukkitMain.getInstance().getMessages();
			PlayerUtils.sendMessage(commandSender, messages.getNotAPlayer());
			return true;
		}

		Player player = (Player) commandSender;
		boolean kitAllowsChange = HGPlayer.from(player).getAttachedListeners().stream()
				.filter(playerAdapter -> playerAdapter instanceof Kit)
				.map(pA -> (Kit) pA)
				.anyMatch(Kit::canChangeKit);

		if (!kitAllowsChange && !ALLOW_MID_GAME_CHANGE && !GameHandler.getInstance().getCurrentPhase().isJoiningEnabled()) {
			PlayerUtils.sendMessage(commandSender, ChatColor.RED + "You cannot change your kit now!");
			return true;
		}

		if (args.length == 0) {
			onOpenKitsMenu(player);
			return true;
		}

		onKitName(player, CommandUtils.joinArgs(args, 0, args.length, " "));
		return true;

	}

	@Override
	public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args) {
		String token = args[args.length - 1].toLowerCase();

		return completeKits(token);
	}

	private void onSyntax(Player player, String alias) {
		PlayerUtils.sendMessage(player, CommandUtils.getSyntax(alias, ImmutableList.of("[<name>]")));
	}

	private void onOpenKitsMenu(Player player) {
		int kitCount = KitsMain.getInstance().getRegisteredKits().size();

		if (kitCount <= 54) {
			InventoryMenu menu = new InventoryMenu(INVENTORY_TITLE, (int) Math.ceil(kitCount / 9d), null, null);
			addKitsToMenu(player, menu);
			menu.open(player);
		} else {
			PagedInvMenu menu = new PagedInvMenu(6, INVENTORY_TITLE, player);
			addKitsToMenu(player, menu);
			menu.openInventory(player);
		}
	}

	private void addKitsToMenu(Player player, InventoryMenu menu) {
		AtomicInteger index = new AtomicInteger(0);
		addKitsToMenu(player, (itemStack, clickHandler) -> {
			menu.addItemAndClickHandler(itemStack, index.getAndIncrement(), clickHandler);
		});
	}

	private void addKitsToMenu(Player player, PagedInvMenu menu) {
		addKitsToMenu(player, menu::addMenuEntry);
	}

	private void addKitsToMenu(Player player, BiConsumer<ItemStack, TerConsumer<Cancellable, ItemStack, InventoryAction>> menuConsumer) {
		KitsMain.getInstance().getDummyKits().stream()
				.sorted(Comparator.comparing(Kit::getDisplayName))
				.sorted((k1, k2) -> {
					int a = k1.canSelect(player) ? 1 : 0;
					int b = k2.canSelect(player) ? 1 : 0;
					return b - a;
				})
				.forEachOrdered(kit -> {
					Function<UUID, Kit> kitFunction = KitsMain.getInstance().getRegisteredKits().get(kit.getClass());
					ItemStack icon = kit.constructIconItemStack(player);
					menuConsumer.accept(icon, handleChooseKit(player, kitFunction));
				});
	}

	private TerConsumer<Cancellable, ItemStack, InventoryAction> handleChooseKit(Player player, Function<UUID, Kit> kitFunction) {
		return (cancellable, itemStack, inventoryAction) -> {
			player.closeInventory();
			applyKit(player, kitFunction);
		};
	}

	private void onKitName(Player player, String arg) {
		Class<? extends Kit> chosen = KitsMain.getInstance().getByAlias(arg);
		if (chosen == null) {
			PlayerUtils.sendMessage(player, ChatColor.RED + "Kit " + ChatColor.WHITE + arg + ChatColor.RED + " not found!");
			return;
		}
		Function<UUID, Kit> kitFunction = KitsMain.getInstance().getRegisteredKits().get(chosen);
		if (kitFunction == null) {
			PlayerUtils.sendMessage(player, ChatColor.RED + "Something went wrong while trying to select your kit. " +
					"Please contact an administrator.");
			return;
		}
		applyKit(player, kitFunction);
	}

	private void applyKit(Player player, Function<UUID, Kit> kitFunction) {
		Kit kit = kitFunction.apply(player.getUniqueId());

		if (!kit.canSelect(player)) {
			PlayerUtils.sendMessage(player, String.format(CANNOT_SELECT_KIT, ChatColor.stripColor(kit.getDisplayName())));
			return;
		}

		HGPlayer hgPlayer = HGPlayer.from(player);
		hgPlayer.getAttachedListeners().removeIf(playerAdapter -> playerAdapter instanceof Kit);

		hgPlayer.getAttachedListeners().add(kit);

		String name = KitsMain.getInstance().getPrimaryKitNameNormalized(kit);
		PlayerUtils.sendMessage(player, ChatColor.GREEN + "You are now " + name + "!");
		KitsMain.getInstance().updatePlayerKitScoreboard(player);

		//If the game has begun, we may give the player their items
		if (GameHandler.getInstance().getCurrentPhase().isJoiningEnabled()) {
			return;
		}
		kit.onBattleReady();
	}

}
