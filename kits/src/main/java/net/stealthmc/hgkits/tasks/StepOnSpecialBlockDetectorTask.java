package net.stealthmc.hgkits.tasks;

import net.stealthmc.hgkits.model.SpecialBlock;
import net.stealthmc.hgkits.specialblocks.SpecialBlockHandler;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
import java.util.Map;

public class StepOnSpecialBlockDetectorTask extends BukkitRunnable {

	@Override
	public void run() {
		Map<Player, List<SpecialBlock>> playersStepped = SpecialBlockHandler.checkProximitiesForPlayingPlayers();
		playersStepped.forEach(this::triggerBlocks);
		SpecialBlockHandler.getSpecialBlocks().removeIf(SpecialBlock::shouldDispose);
	}

	private void triggerBlocks(Player player, List<SpecialBlock> blocks) {
		blocks.forEach(block -> block.onStepOver(player));
	}

}
