package net.stealthmc.hgkits.specialblocks;

import net.stealthmc.hgkits.model.SpecialBlock;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.UUID;

public class DisappearingSpecialBlock extends SpecialBlock {

	private boolean disappeared = false;

	public DisappearingSpecialBlock(Block block, UUID blockOwner) {
		super(block, blockOwner);
		detectionRadiusSquared = 2;
	}

	@Override
	public void onStepOver(Player stepped) {
		if (disappeared || stepped.getUniqueId().equals(this.getBlockOwner())) {
			return;
		}
		this.getBlock().setType(Material.AIR);
		disappeared = true;
	}

	@Override
	public boolean shouldDispose() {
		return super.getBlock().getType() == Material.AIR || disappeared;
	}
}
