# stealthmc-HG
A mini-game called Hardcore Games, with extra modules that add for example kits!

## Requirements
* [Java JDK 1.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html "Java 1.8")
* [IntelliJ IDEA](https://www.jetbrains.com/idea/download/ "IntelliJ IDEA")
* [Git](https://git-scm.com/downloads "Git")
* Maven (Integrated in IntelliJ)
* IntelliJ Settings [(Click me)](https://cdn.discordapp.com/attachments/520934423626121217/631477062451855360/IntelijSettings.zip "(Click me)")
* Basic knowledge how IntelliJ and coding in general works.

#### Maven Compile command
```
clean install
```
After compiling you will get 2 jars in different folders.
```
./core/target/stealthmc-hg-core-1.0.jar
./kits/target/stealthmc-hg-kits-1.0.jar
```
There is a  ```Copy Plugins.bat``` that will copy the plugins into the main folder so you can use them faster ;)

## Server Requirements
* [Java JRE 1.8](https://java.com/de/download/ "Java JRE 1.8")
* [Paper Spigot](https://papermc.io/legacy "Paper Spigot") (1.14/15 Paper only Support)
* Basic knowledge on how Minecraft server works ^^

## Foreign dependency information (Outdated)
To easier access and modify Item Attributes, [ItemAttributeAPI](https://www.spigotmc.org/resources/item-attribute-modifier-api-library.22747/) 
is used. <br>
The artifact is available for maven use in the `https://nexus.reallifesucks.de/repository/public/` 
##### Repository:
```xml
<dependency>
    <groupId>me.michel_0</groupId>
    <artifactId>ItemAttributeAPI</artifactId>
    <version>1.0</version>
</dependency>
```